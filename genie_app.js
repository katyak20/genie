var express = require('express');
var util = require('util');
const url = require('url');
var path = require('path');
var http = require('http');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var app = express();

result='';
var clickCount = 0;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

var server = http.createServer(app);
var io = require('socket.io').listen(server);

var cp = require('child_process');
var child;

io.sockets.on('connection', function(client) {
    var timesliceFSWatcher, entsFSWatcher, embmFSWatcher, goldFSWatcher, gsicFSWatcher;
    var fsWaitAnnual = false;
    var fsWaitSlice = false;
    var socketId = client.id;
    console.log('Client connected...');

    client.on("disconnect", function () {
        console.log("                   Client disconnected!! with id " + client.id);

    })

    client.on('clicked', function(form_data) {
       process.chdir('/home/phil_holden/node_app/genie/genie-main');
       console.log(form_data);
       child = cp.spawn("./genie.job", ['-f', 'configs/teaching_tool_exp.xml', form_data.diff1, form_data.diff2, form_data.adrag, form_data.diffsic, form_data.diffamp1, form_data.diffamp2, form_data.scl_fwf, form_data.olradj, form_data.diffwid, form_data.k7, form_data.k14, form_data.k17, form_data.k18, form_data.k20, form_data.k24, form_data.k26, form_data.k29, form_data.k32, form_data.kz, form_data.emit0, form_data.emit1], {detached: true});
       child.on('message', function (data) {
            console.log('message'  + data);
        });
        child.on('error', function (data) {
            console.log('error'  + data);
        });
        child.stderr.on('data',function(data){
            console.log("Child_process Errors: " + data);

        });
        child.stdout.on('data', function (data) {
            result += data;
            /*io.emit('buttonUpdate', data.toString());
            if (true) {
                child.stdin.write("MB16" + "\n" + "1" + "\n" + "1" + "\n" + "1" + "\n" + "1" + "\n" + "1" + "\n");
            };*/
            // io.emit("tweet", data.toString());
        });
        child.on('close', function () {
            console.log('Genie child process done');
           // io.emit('genie_process_finished', "Genie child process closed");
        });
        child.on('uncaughtException', function (err) {
            console.log("ERROR");
            console.log(util.inspect(err));
        });

    });
        var fsWaitEnts = false;


      entsFSWatcher = fs.watch('/home/phil_holden/node_app/genie_output/teaching_tool_exp/ents/', function (event, filename) {
            if (filename === "ents.nc") {
                if (event ==='change') {
                    if (fsWaitEnts) return;
                    fsWaitEnts = setTimeout(() => {
                        fsWaitEnts = false;
                    }, 200);

                    var writeStream = fs.createWriteStream('/home/phil_holden/node_app/public/javascripts/ents.json');
                    process.chdir('/home/phil_holden/node_app/genie_output/teaching_tool_exp/ents');
                    // var ncdump_child = cp.spawn("ncdump-json", ['-v', 'fv,land_temp,land_water,leaf,photosynthesis,snow,soil_resp,soilC,veg_resp,vegC', '-j', 'ents.nc']);
                    var ncdump_child = cp.spawn("ncdump-json", ['-v', 'time,month,year,fv,land_temp,land_water,leaf,photosynthesis,snow,soil_resp,soilC,veg_resp,vegC', '-j', 'ents.nc']);
                    ncdump_child.stdout.setEncoding('utf8');
                    ncdump_child.stdout.pipe(writeStream);
                    ncdump_child.stderr.on('data', function (data) {
                        console.log('stderr: ' + data);
                    });
                }
            }
            else if(filename === "ents0.nc") {}
            else {
                return;
            }
        });
       var fsWaitEmbm=false;
       embmFSWatcher = fs.watch('/home/phil_holden/node_app/genie_output/teaching_tool_exp/embm/embm.nc', function (event, filename) {
            //console.log(filename + "   " + event);
            if (filename && event ==='change') {
                if (fsWaitEmbm) return;
                fsWaitEmbm = setTimeout(() => {
                    fsWaitEmbm = false;
                }, 200);
                //console.log(event + "               " + filename);
                var writeStream = fs.createWriteStream('/home/phil_holden/node_app/public/javascripts/embm.json');

                process.chdir('/home/phil_holden/node_app/genie_output/teaching_tool_exp/embm');
                var ncdump_embm_child = cp.spawn("ncdump-json", ['-v', 'time,month,year,air_temp,evap,humidity,pptn', '-j', 'embm.nc']);

                ncdump_embm_child.stdout.setEncoding('utf8');

                ncdump_embm_child.stdout.pipe(writeStream);

                ncdump_embm_child.stderr.on('data', function (data) {
                    console.log('stderr: ' + data);
                });

            }
            else {
                console.log('filename embm.nc not provided');
            }
        });
        var fsWaitGold = false;
        goldFSWatcher = fs.watch('/home/phil_holden/node_app/genie_output/teaching_tool_exp/goldstein/gold.nc', function (event, filename) {

            if (filename && event ==='change') {
                if (fsWaitGold) return;
                fsWaitGold = setTimeout(() => {
                    fsWaitGold = false;
                }, 200);
               // console.log(event + "               " + filename);
                var writeStream = fs.createWriteStream('/home/phil_holden/node_app/public/javascripts/goldstein.json');

                process.chdir('/home/phil_holden/node_app/genie_output/teaching_tool_exp/goldstein');
                var ncdump_gold_child = cp.spawn("ncdump-json", ['-v', 'time,month,year,depth,temp,salinity,density,bathymetry', '-j', 'gold.nc']);

                ncdump_gold_child.stdout.setEncoding('utf8');

                ncdump_gold_child.stdout.pipe(writeStream);

                ncdump_gold_child.stderr.on('data', function (data) {
                    console.log('stderr: ' + data);
                });

            }
            else {
                console.log('filename gold.nc not provided');
            }
        });
        var fsWaitGoldSeaIce = false;

      gsicFSWatcher = fs.watch('/home/phil_holden/node_app/genie_output/teaching_tool_exp/goldsteinseaice/gsic.nc', function (event, filename) {

            if (filename && event ==='change') {
                if (fsWaitGoldSeaIce) return;
                fsWaitGoldSeaIce = setTimeout(() => {
                    fsWaitGoldSeaIce = false;
                }, 200);
               // console.log(event + "               " + filename);
                var writeStream = fs.createWriteStream('/home/phil_holden/node_app/public/javascripts/gsic.json');

                process.chdir('/home/phil_holden/node_app/genie_output/teaching_tool_exp/goldsteinseaice');
                var ncdump_gsic_child = cp.spawn("ncdump-json", ['-v', 'time,month,year,sic_temp,sic_height,sic_cover', '-j', 'gsic.nc']);

                ncdump_gsic_child.stdout.setEncoding('utf8');

                ncdump_gsic_child.stdout.pipe(writeStream);

                ncdump_gsic_child.stderr.on('data', function (data) {
                    console.log('stderr: ' + data);
                });

            }
            else {
                console.log('filename gsic.nc not provided');
            }
        });


    //var fsWaitSlice = false;
    timesliceFSWatcher =fs.watch('/home/phil_holden/node_app/genie_output/teaching_tool_exp/', function (event, filename) {

        var previous_data;
        var previous__annual_data;
        if (filename === "time_slice.txt") {

            //  console.log(filename + "   " + event);
            if (event ==='change') {
                if (fsWaitSlice) return;
                fsWaitSlice = setTimeout(() => {
                    fsWaitSlice = false;
                }, 200);

                fs.readFile('/home/phil_holden/node_app/genie_output/teaching_tool_exp/time_slice.txt',  "utf8", function read(err, data) {
                    if (err) {
                        throw err;
                    }
                    var string_data = data.toString();

                    if (string_data != previous_data) {
                        console.log(event + "               " + filename);
                        io.to(socketId).emit('timeslice_changed', data);
                        previous_data = data.toString();
                    }
                });
            }
        }
        else if (filename === "ann_slice.txt") {
            if (event ==='change') {
                if (fsWaitAnnual) return;
                fsWaitAnnual = setTimeout(() => {
                    fsWaitAnnual = false;
                }, 200);

                fs.readFile('/home/phil_holden/node_app/genie_output/teaching_tool_exp/ann_slice.txt',  "utf8", function read(err, data) {
                    if (err) {
                        throw err;
                    }
                    var string_data = data.toString();

                    if (string_data!=previous__annual_data) {
                        // console.log(data);
                        io.to(socketId).emit('annual_slice_changed', data);
                        previous__annual_data=data.toString();
                    }
                });
            }
        }
        else {
            return false;
        }
    });

  client.on('stop', function(){
     console.log("Stop message has been received for pid!!" + child.pid);

     console.log("client.id           " + client.id);
     console.log("socketId          " + socketId);

     if (client.id ===socketId) {
         console.log("client.id ===socketId              TRUE!!");

         timesliceFSWatcher.close();
         entsFSWatcher.close();
         embmFSWatcher.close();
         goldFSWatcher.close();
         gsicFSWatcher.close();

         try{
             process.kill(-child.pid, 'SIGTERM');
             process.kill(-child.pid, 'SIGKILL');
         }
         catch(e) {console.log(e);}
         client.disconnect();
     }

  });

});

module.exports = app;

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');

});

server.listen(process.env.PORT || 3000, function(){
    console.log('listening on *:3000');
});

