% GOLDSTEIN ocean model, FW and heat fluxes, first data line is region area (m2)    
% time                                         Total FW                                                             Total heat                            
%                     Global      Atlantic       Pacific        Indian      Southern        Global      Atlantic       Pacific        Indian      Southern
%                         Sv            Sv            Sv            Sv            Sv            PW            PW            PW            PW            PW
  0.000000E+00  0.367477E+15  0.834103E+14  0.140066E+15  0.472134E+14  0.967874E+14  0.367477E+15  0.834103E+14  0.140066E+15  0.472134E+14  0.967874E+14
  0.100000E+03  0.192917E-02 -0.196229E+00  0.747715E-01 -0.117126E+00  0.240512E+00  0.190157E+00  0.154362E-01  0.156100E+01  0.855937E+00 -0.224222E+01
