 Atmospheric pCO2             :    921.842 uatm           <-> 0.1630923E+18 mol
 Atmospheric pO2              : 209568.041 uatm           <-> 0.3707678E+20 mol
 Ocean temp                   :      2.182 degrees C
 Ocean sal                    :     34.901 PSU
 Ocean DIC                    :   2297.541 umol kg-1      <-> 0.3075300E+19 mol
 Ocean PO4                    :      2.147 umol kg-1      <-> 0.2873906E+16 mol
 Ocean Fe                     :      0.000 umol kg-1      <-> 0.3396100E+11 mol
 Ocean O2                     :    162.648 umol kg-1      <-> 0.2177079E+18 mol
 Ocean ALK                    :   2363.258 umol kg-1      <-> 0.3163264E+19 mol
 Ocean DOM_C                  :      1.384 umol kg-1      <-> 0.1852797E+16 mol
 Ocean DOM_P                  :      0.012 umol kg-1      <-> 0.1583587E+14 mol
 Ocean DOM_Fe                 :      0.000 umol kg-1      <-> 0.1423392E+11 mol
 Ocean FeL                    :      0.001 umol kg-1      <-> 0.8731578E+12 mol
 Ocean L                      :      0.000 umol kg-1      <-> 0.4654000E+12 mol
 Ocean Ca                     :  10250.301 umol kg-1      <-> 0.1372022E+20 mol
 Export flux POC              :    222.249 umol cm-2 yr-1 <-> 0.8167158E+15 mol yr-1
 Export flux CaCO3            :     10.178 umol cm-2 yr-1 <-> 0.3740026E+14 mol yr-1
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
