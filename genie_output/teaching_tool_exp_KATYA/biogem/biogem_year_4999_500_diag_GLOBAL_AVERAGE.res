 ==========================
 GLOBAL DIAGNOSTICS
 ==========================
  
 Year ............... :    4999.500
 Integration interval :       0.021    yr
  
 --------------------------
 MISCELLANEOUS PROPERTIES
  
 Global ocean k = n_k (surface) area ............0.3674774E+15 m2
 Global ocean k = (n_k - 1) (base of surface laye0.3659036E+15 m2
 Global ocean volume ......................... : 0.1302484E+19 m3
 Global mean air-sea coefficient, K(CO2) ..... : 0.026151     mol m-2 yr-1 uatm-1
  
 --------------------------
 ATMOSPHERIC PROPERTIES
  
 Atmospheric pCO2             :    921.842 uatm
 Atmospheric pO2              : 209568.041 uatm
  
 --------------------------
 BULK OCEAN PROPERTIES
  
 Ocean temp             ..... :      2.182 degrees C
 Ocean sal              ..... :     34.901 PSU
 Ocean DIC              ..... :   2297.541 umol kg-1 <-> 0.3075300E+19 mol
 Ocean PO4              ..... :      2.147 umol kg-1 <-> 0.2873906E+16 mol
 Ocean Fe               ..... :      0.000 umol kg-1 <-> 0.3396100E+11 mol
 Ocean O2               ..... :    162.648 umol kg-1 <-> 0.2177079E+18 mol
 Ocean ALK              ..... :   2363.258 umol kg-1 <-> 0.3163264E+19 mol
 Ocean DOM_C            ..... :      1.384 umol kg-1 <-> 0.1852797E+16 mol
 Ocean DOM_P            ..... :      0.012 umol kg-1 <-> 0.1583587E+14 mol
 Ocean DOM_Fe           ..... :      0.000 umol kg-1 <-> 0.1423392E+11 mol
 Ocean FeL              ..... :      0.001 umol kg-1 <-> 0.8731578E+12 mol
 Ocean L                ..... :      0.000 umol kg-1 <-> 0.4654000E+12 mol
 Ocean Ca               ..... :  10250.301 umol kg-1 <-> 0.1372022E+20 mol
  
 --------------------------
 BULK OCEAN CARBONATE CHEMSITRY
  
 Carb chem conc_CO2         . :     39.843 umol kg-1 <-> 0.5333019E+17 mol
 Carb chem conc_CO3         . :     66.158 umol kg-1 <-> 0.8855349E+17 mol
 Carb chem conc_HCO3        . :   2191.519 umol kg-1 <-> 0.2933389E+19 mol
  
 ------------------------------
 SURFACE EXPORT PRODUCTION
  
 Export flux POC              :    222.249 umol cm-2 yr-1 <-> 0.8167158E+15 mol yr-1
 Export flux CaCO3            :     10.178 umol cm-2 yr-1 <-> 0.3740026E+14 mol yr-1
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
  
 ------------------------------
 SEDIMENTATION
  
 Export flux POC              :     23.179 umol cm-2 yr-1 <-> 0.8517782E+14 mol yr-1
 Export flux CaCO3            :      6.400 umol cm-2 yr-1 <-> 0.2351995E+14 mol yr-1
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
  
 ------------------------------
 CARBON FLUX SUMMARY
  
 Total POC export   :   0.8167158E+15 mol yr-1 =   9.801 GtC yr-1
 Total CaCO3 export :   0.3740026E+14 mol yr-1 =   0.449 GtC yr-1
  
 Total POC rain     :   0.8517782E+14 mol yr-1 =   1.022 GtC yr-1
 Total CaCO3 rain   :   0.2351995E+14 mol yr-1 =   0.282 GtC yr-1
