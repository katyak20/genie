 Atmospheric pCO2             :  22837.766 uatm           <-> 0.4040458E+19 mol
 Atmospheric pO2              : 209598.370 uatm           <-> 0.3708214E+20 mol
 Ocean temp                   :      2.893 degrees C
 Ocean sal                    :     34.901 PSU
 Ocean DIC                    :   2442.915 umol kg-1      <-> 0.3269900E+19 mol
 Ocean PO4                    :      2.146 umol kg-1      <-> 0.2872550E+16 mol
 Ocean Fe                     :      0.000 umol kg-1      <-> 0.3746434E+11 mol
 Ocean O2                     :    158.811 umol kg-1      <-> 0.2125718E+18 mol
 Ocean ALK                    :   2363.275 umol kg-1      <-> 0.3163300E+19 mol
 Ocean DOM_C                  :      1.622 umol kg-1      <-> 0.2171431E+16 mol
 Ocean DOM_P                  :      0.014 umol kg-1      <-> 0.1855924E+14 mol
 Ocean DOM_Fe                 :      0.000 umol kg-1      <-> 0.1704114E+11 mol
 Ocean FeL                    :      0.001 umol kg-1      <-> 0.8900231E+12 mol
 Ocean L                      :      0.000 umol kg-1      <-> 0.4485345E+12 mol
 Ocean Ca                     :  10250.262 umol kg-1      <-> 0.1372022E+20 mol
 Export flux POC              :    176.773 umol cm-2 yr-1 <-> 0.6496009E+15 mol yr-1
 Export flux CaCO3            :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
