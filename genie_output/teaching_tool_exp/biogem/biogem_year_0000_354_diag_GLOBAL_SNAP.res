 Atmospheric pCO2             :    276.211 uatm           <-> 0.4886725E+17 mol
 Atmospheric pO2              : 209533.107 uatm           <-> 0.3707060E+20 mol
 Ocean temp                   :      1.605 degrees C
 Ocean sal                    :     34.902 PSU
 Ocean DIC                    :   2242.622 umol kg-1      <-> 0.3001708E+19 mol
 Ocean PO4                    :      2.148 umol kg-1      <-> 0.2875255E+16 mol
 Ocean Fe                     :      0.000 umol kg-1      <-> 0.3545625E+11 mol
 Ocean O2                     :    167.099 umol kg-1      <-> 0.2236585E+18 mol
 Ocean ALK                    :   2363.299 umol kg-1      <-> 0.3163232E+19 mol
 Ocean DOM_C                  :      1.369 umol kg-1      <-> 0.1832631E+16 mol
 Ocean DOM_P                  :      0.012 umol kg-1      <-> 0.1566351E+14 mol
 Ocean DOM_Fe                 :      0.000 umol kg-1      <-> 0.1387734E+11 mol
 Ocean FeL                    :      0.001 umol kg-1      <-> 0.8845372E+12 mol
 Ocean L                      :      0.000 umol kg-1      <-> 0.4540204E+12 mol
 Ocean Ca                     :  10250.577 umol kg-1      <-> 0.1372021E+20 mol
 Export flux POC              :    371.545 umol cm-2 yr-1 <-> 0.1365345E+16 mol yr-1
 Export flux CaCO3            :     45.685 umol cm-2 yr-1 <-> 0.1678838E+15 mol yr-1
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
