 % time (yr) / global pCO2 flux (mol yr-1) / global pCO2 density (mol m-2 yr-1)  NOTE: includes the net flux associated with any flux or restoring forcing of atmospheric composition  and is thus not necessarily the same as the air-sea flux.
       0.500  0.8437508E+13       0.023
       1.500  0.3703915E+13       0.010
       2.500  0.4735301E+13       0.013
       3.500  0.4710003E+13       0.013
