 Atmospheric pCO2             :        NaN uatm           <->           NaN mol
 Atmospheric pO2              : 209533.875 uatm           <-> 0.3707073E+20 mol
 Ocean temp                   :        NaN degrees C
 Ocean sal                    :        NaN PSU
 Ocean DIC                    :        NaN umol kg-1      <->           NaN mol
 Ocean PO4                    :        NaN umol kg-1      <->           NaN mol
 Ocean Fe                     :        NaN umol kg-1      <->           NaN mol
 Ocean O2                     :        NaN umol kg-1      <->           NaN mol
 Ocean ALK                    :        NaN umol kg-1      <->           NaN mol
 Ocean DOM_C                  :        NaN umol kg-1      <->           NaN mol
 Ocean DOM_P                  :        NaN umol kg-1      <->           NaN mol
 Ocean DOM_Fe                 :        NaN umol kg-1      <->           NaN mol
 Ocean FeL                    :        NaN umol kg-1      <->           NaN mol
 Ocean L                      :        NaN umol kg-1      <->           NaN mol
 Ocean Ca                     :        NaN umol kg-1      <->           NaN mol
 Export flux POC              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux CaCO3            :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux det              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux opal             :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
