 Atmospheric pCO2             :        NaN uatm           <->           NaN mol
 Atmospheric pO2              :        NaN uatm           <->           NaN mol
 Ocean temp                   :      1.599 degrees C
 Ocean sal                    :     34.902 PSU
 Ocean DIC                    :        NaN umol kg-1      <->           NaN mol
 Ocean PO4                    :      2.150 umol kg-1      <-> 0.2877460E+16 mol
 Ocean Fe                     :      0.000 umol kg-1      <-> 0.3645739E+11 mol
 Ocean O2                     :        NaN umol kg-1      <->           NaN mol
 Ocean ALK                    :   2363.257 umol kg-1      <-> 0.3163209E+19 mol
 Ocean DOM_C                  :      1.200 umol kg-1      <-> 0.1606032E+16 mol
 Ocean DOM_P                  :      0.010 umol kg-1      <-> 0.1372677E+14 mol
 Ocean DOM_Fe                 :      0.000 umol kg-1      <-> 0.1211716E+11 mol
 Ocean FeL                    :      0.001 umol kg-1      <-> 0.8860040E+12 mol
 Ocean L                      :      0.000 umol kg-1      <-> 0.4525536E+12 mol
 Ocean Ca                     :  10250.475 umol kg-1      <-> 0.1372021E+20 mol
 Export flux POC              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux CaCO3            :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
