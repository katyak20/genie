 % time (yr) / global pO2 sea->air transfer flux (mol yr-1) / global pO2 density (mol m-2 yr-1)
       0.500 -0.1371009E+14      -0.037
       1.500  0.6094582E+13       0.017
       2.500 -0.1328174E+13      -0.004
       3.500 -0.6973496E+12      -0.002
