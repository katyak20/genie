 ==========================
 GLOBAL DIAGNOSTICS
 ==========================
  
 Year ............... :    4999.500
 Integration interval :       0.021    yr
  
 --------------------------
 MISCELLANEOUS PROPERTIES
  
 Global ocean k = n_k (surface) area ............0.3674774E+15 m2
 Global ocean k = (n_k - 1) (base of surface laye0.3659036E+15 m2
 Global ocean volume ......................... : 0.1302484E+19 m3
 Global mean air-sea coefficient, K(CO2) ..... :      NaN     mol m-2 yr-1 uatm-1
  
 --------------------------
 ATMOSPHERIC PROPERTIES
  
 Atmospheric pCO2             :        NaN uatm
 Atmospheric pO2              : 209534.605 uatm
  
 --------------------------
 BULK OCEAN PROPERTIES
  
 Ocean temp             ..... :        NaN degrees C
 Ocean sal              ..... :        NaN PSU
 Ocean DIC              ..... :        NaN umol kg-1 <->           NaN mol
 Ocean PO4              ..... :        NaN umol kg-1 <->           NaN mol
 Ocean Fe               ..... :        NaN umol kg-1 <->           NaN mol
 Ocean O2               ..... :        NaN umol kg-1 <->           NaN mol
 Ocean ALK              ..... :        NaN umol kg-1 <->           NaN mol
 Ocean DOM_C            ..... :        NaN umol kg-1 <->           NaN mol
 Ocean DOM_P            ..... :        NaN umol kg-1 <->           NaN mol
 Ocean DOM_Fe           ..... :        NaN umol kg-1 <->           NaN mol
 Ocean FeL              ..... :        NaN umol kg-1 <->           NaN mol
 Ocean L                ..... :        NaN umol kg-1 <->           NaN mol
 Ocean Ca               ..... :        NaN umol kg-1 <->           NaN mol
  
 --------------------------
 BULK OCEAN CARBONATE CHEMSITRY
  
 Carb chem conc_CO2         . :        NaN umol kg-1 <->           NaN mol
 Carb chem conc_CO3         . :        NaN umol kg-1 <->           NaN mol
 Carb chem conc_HCO3        . :        NaN umol kg-1 <->           NaN mol
  
 ------------------------------
 SURFACE EXPORT PRODUCTION
  
 Export flux POC              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux CaCO3            :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux det              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux opal             :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
  
 ------------------------------
 SEDIMENTATION
  
 Export flux POC              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux CaCO3            :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux det              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux opal             :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
  
 ------------------------------
 CARBON FLUX SUMMARY
  
 Total POC export   :   0.0000000E+00 mol yr-1 =   0.000 GtC yr-1
 Total CaCO3 export :   0.0000000E+00 mol yr-1 =   0.000 GtC yr-1
  
 Total POC rain     :   0.0000000E+00 mol yr-1 =   0.000 GtC yr-1
 Total CaCO3 rain   :   0.0000000E+00 mol yr-1 =   0.000 GtC yr-1
