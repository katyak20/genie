 % time (yr) / global sea-ice area (m2) / mean sea-ice cover (%) / global sea-ice volume (m3) / mean sea-ice thickness (m)
       0.500  0.3012E+14    8.197  0.4299E+14    1.500
       1.500  0.3019E+14    8.214  0.4303E+14    1.499
       2.500  0.3015E+14    8.205  0.4298E+14    1.498
       3.500  0.3019E+14    8.215  0.4300E+14    1.493
