 =================================
 === GLOBAL SEDIMENT DIAG DATA ===
 =================================
  
 --- DEEP-SEA SEDIMENT GRID ------
  
 ---------------------------------
 Total # deep-sea grid pts :   914
 Total deep-sea area       :  0.359608E+15 m2
 ---------------------------------
 POC rain                  :  0.804191E+14 mol yr-1 =   0.965 GtC yr-1
 POC diss                  :  0.804191E+14 mol yr-1 =   0.965 GtC yr-1
 Total POC pres            :  0.156250E-01 mol yr-1 =   0.000 GtC yr-1   =    0.00 %
 Mean wt% POC              :  0.00 %
 ---------------------------------
 CaCO3 rain                :  0.713958E+14 mol yr-1 =   0.857 GtC yr-1
 CaCO3 diss                :  0.603498E+14 mol yr-1 =   0.724 GtC yr-1
 Total CaCO3 pres          :  0.110459E+14 mol yr-1 =   0.133 GtC yr-1   =   15.47 %
 Mean wt% CaCO3            : 25.89 %
 ---------------------------------
 CaCO3/POC rain ratio      :  0.888
 ---------------------------------
 opal rain                 :  0.114642E+14    mol yr-1
 opal diss                 :  0.000000E+00    mol yr-1
 Total opal pres           :  0.114642E+14 mol yr-1 =                       100.00 %
 Mean wt% opal             : 36.15 %
 ---------------------------------
  
 --- REEF SEDIMENT GRID ----------
  
 ---------------------------------
 Total # reef grid pts     :     0
 Total reef area           :  0.000000E+00 m2
 Active # reef grid pts    :     0
 Active reef area          :  0.000000E+00 m2
 ---------------------------------
 CaCO3 rain                :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1
 CaCO3 diss                :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1
 Total CaCO3 pres          :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1   =    0.00 %
 Mean wt% CaCO3            :  0.00 %
 ---------------------------------
 Mean weighted d13C CaCO3  :  0.00 o/oo
 ---------------------------------
  
 --- SHALLOW SEDIMENT GRID -------
  
 ---------------------------------
 Total # grid pts          :     0
 Total area                :  0.000000E+00 m2
 ---------------------------------
 ---------------------------------
 POC rain                  :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1
 POC diss                  :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1
 Total POC pres            :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1   =    0.00 %
 Mean wt% POC              :  0.00 %
 ---------------------------------
 CaCO3 rain                :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1
 CaCO3 diss                :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1
 Total CaCO3 pres          :  0.000000E+00 mol yr-1 =   0.000 GtC yr-1   =    0.00 %
 Mean wt% CaCO3            :  0.00 %
 ---------------------------------
 CaCO3/POC rain ratio      :  0.000
 ---------------------------------
 opal rain                 :  0.000000E+00    mol yr-1
 opal diss                 :  0.000000E+00    mol yr-1
 Total opal pres           :  0.000000E+00 mol yr-1 =                         0.00 %
 Mean wt% opal             :  0.00 %
 ---------------------------------
  
 --- TOTAL SEDIMENT GRID ---------
 --- (equivalent to ocean grid) --
  
 ---------------------------------
 Total # sediment grid pts :   914
 Total sediment area       :  0.359608E+15 m2
  
