 
 =========== Global Weathering @ year         5000.5  ===========
 
 --------------------------- inputs -----------------------------
 
global average land surface temperature (deg C)   :     16.409789
max land surface temperature (deg C)              :     40.566881
min land surface temperature (deg C)              :    -63.819754
global average land surface runoff (mm/s)         :  3.147993E-05
max land surface runoff (mm/s)                    :  1.175389E-04
min land surface runoff (mm/s)                    :  0.000000E+00
global av land surf productivity (kgC m-2 yr-1)   :      1.156693
max land surface productivity (kgC m-2 yr-1)      :      5.194336
min land surface productivity (kgC m-2 yr-1)      :      0.000000
global av atmospheric pCO2 (ppm)                  :    277.713465
max atmospheric pCO2 (ppm)                        :    277.713465
min atmospheric pCO2 (ppm)                        :      0.000000
 
 --------------------------- outputs ----------------------------
 
loc_weather_ratio_CaCO3                           :      1.000000
loc_weather_ratio_CaSiO3                          :      1.000000
weather_fCaCO3 (mol yr-1)                         :  1.000000E+13
weather_fCaSiO3 (mol yr-1)                        :  0.000000E+00
CO2 weathering flux (mol yr-1)                    :  0.000000E+00
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
 
                            * land * 
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
                            * ocean * 
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
 
 
