 
 =========== Global Weathering @ year            8.5  ===========
 
 --------------------------- inputs -----------------------------
 
global average land surface temperature (deg C)   :     -3.780517
max land surface temperature (deg C)              :     18.971206
min land surface temperature (deg C)              :    -63.327761
global average land surface runoff (mm/s)         :  1.378189E-05
max land surface runoff (mm/s)                    :  4.128145E-05
min land surface runoff (mm/s)                    :  0.000000E+00
global av land surf productivity (kgC m-2 yr-1)   :      0.295103
max land surface productivity (kgC m-2 yr-1)      :      1.985365
min land surface productivity (kgC m-2 yr-1)      :      0.000000
global av atmospheric pCO2 (ppm)                  :    277.502658
max atmospheric pCO2 (ppm)                        :    277.502658
min atmospheric pCO2 (ppm)                        :      0.000000
 
 --------------------------- outputs ----------------------------
 
loc_weather_ratio_CaCO3                           :      1.000000
loc_weather_ratio_CaSiO3                          :      1.000000
weather_fCaCO3 (mol yr-1)                         :  1.000000E+13
weather_fCaSiO3 (mol yr-1)                        :  0.000000E+00
CO2 weathering flux (mol yr-1)                    :  0.000000E+00
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
 
                            * land * 
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
                            * ocean * 
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
 
 
