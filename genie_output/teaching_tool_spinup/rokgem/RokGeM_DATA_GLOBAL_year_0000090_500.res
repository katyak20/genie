 
 =========== Global Weathering @ year           90.5  ===========
 
 --------------------------- inputs -----------------------------
 
global average land surface temperature (deg C)   :     10.837118
max land surface temperature (deg C)              :     34.442084
min land surface temperature (deg C)              :    -66.031721
global average land surface runoff (mm/s)         :  2.505696E-05
max land surface runoff (mm/s)                    :  9.448771E-05
min land surface runoff (mm/s)                    :  0.000000E+00
global av land surf productivity (kgC m-2 yr-1)   :      0.810826
max land surface productivity (kgC m-2 yr-1)      :      4.120160
min land surface productivity (kgC m-2 yr-1)      :      0.000000
global av atmospheric pCO2 (ppm)                  :    277.733909
max atmospheric pCO2 (ppm)                        :    277.733909
min atmospheric pCO2 (ppm)                        :      0.000000
 
 --------------------------- outputs ----------------------------
 
loc_weather_ratio_CaCO3                           :      1.000000
loc_weather_ratio_CaSiO3                          :      1.000000
weather_fCaCO3 (mol yr-1)                         :  1.000000E+13
weather_fCaSiO3 (mol yr-1)                        :  0.000000E+00
CO2 weathering flux (mol yr-1)                    :  0.000000E+00
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
 
                            * land * 
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
                            * ocean * 
ALK weathering flux (mol yr-1)                    :  2.000000E+13
DIC weathering flux (mol yr-1)                    :  1.000000E+13
Ca weathering flux (mol yr-1)                     :  1.000000E+13
DIC_13C weathering flux (mol yr-1)                :  1.107791E+11
 
 
