 Atmospheric pCO2             :    278.046 uatm           <-> 0.4919181E+17 mol
 Atmospheric pCO2_13C         :     -6.651 o/oo
 Atmospheric pCO2_14C         :     37.227 o/oo
 Atmospheric pO2              : 209533.848 uatm           <-> 0.3707073E+20 mol
 Ocean temp                   :      1.598 degrees C
 Ocean sal                    :     34.902 PSU
 Ocean DIC                    :   2242.771 umol kg-1      <-> 0.3001805E+19 mol
 Ocean DIC_13C                :      0.214 o/oo     
 Ocean DIC_14C                :   -135.573 o/oo     
 Ocean PO4                    :      2.149 umol kg-1      <-> 0.2876852E+16 mol
 Ocean Fe                     :      0.000 umol kg-1      <-> 0.3637986E+11 mol
 Ocean O2                     :    166.775 umol kg-1      <-> 0.2232172E+18 mol
 Ocean ALK                    :   2363.256 umol kg-1      <-> 0.3163067E+19 mol
 Ocean DOM_C                  :      1.225 umol kg-1      <-> 0.1639670E+16 mol
 Ocean DOM_C_13C              :    -22.294 o/oo     
 Ocean DOM_C_14C              :   -118.621 o/oo     
 Ocean DOM_P                  :      0.010 umol kg-1      <-> 0.1401428E+14 mol
 Ocean DOM_Fe                 :      0.000 umol kg-1      <-> 0.1237547E+11 mol
 Ocean FeL                    :      0.001 umol kg-1      <-> 0.8856079E+12 mol
 Ocean L                      :      0.000 umol kg-1      <-> 0.4528883E+12 mol
 Ocean Ca                     :  10250.460 umol kg-1      <-> 0.1371958E+20 mol
 Export flux POC              :    256.883 umol cm-2 yr-1 <-> 0.9439860E+15 mol yr-1
 Export flux POC_13C          :    -25.212 o/oo     
 Export flux POC_14C          :   -137.039 o/oo     
 Export flux CaCO3            :     30.795 umol cm-2 yr-1 <-> 0.1131642E+15 mol yr-1
 Export flux CaCO3_13C        :      2.729 o/oo     
 Export flux CaCO3_14C        :    -81.689 o/oo     
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
