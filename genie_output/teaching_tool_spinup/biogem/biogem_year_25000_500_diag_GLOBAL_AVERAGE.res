 ==========================
 GLOBAL DIAGNOSTICS
 ==========================
  
 Year ............... :   25000.500
 Integration interval :       1.000    yr
  
 --------------------------
 MISCELLANEOUS PROPERTIES
  
 Global ocean k = n_k (surface) area ............0.3674774E+15 m2
 Global ocean k = (n_k - 1) (base of surface laye0.3659036E+15 m2
 Global ocean volume ......................... : 0.1302484E+19 m3
 Global mean air-sea coefficient, K(CO2) ..... : 0.026101     mol m-2 yr-1 uatm-1
  
 --------------------------
 ATMOSPHERIC PROPERTIES
  
 Atmospheric pCO2             :    278.001 uatm
 Atmospheric pCO2_13C         :     -6.503 o/oo
 Atmospheric pCO2_14C         :     37.360 o/oo
 Atmospheric pO2              : 209534.143 uatm
  
 --------------------------
 BULK OCEAN PROPERTIES
  
 Ocean temp             ..... :      1.597 degrees C
 Ocean sal              ..... :     34.902 PSU
 Ocean DIC              ..... :   2242.778 umol kg-1 <-> 0.3001814E+19 mol
 Ocean DIC_13C          ..... :      0.214 o/oo     
 Ocean DIC_14C          ..... :   -135.572 o/oo     
 Ocean PO4              ..... :      2.149 umol kg-1 <-> 0.2876828E+16 mol
 Ocean Fe               ..... :      0.000 umol kg-1 <-> 0.3627604E+11 mol
 Ocean O2               ..... :    166.746 umol kg-1 <-> 0.2231783E+18 mol
 Ocean ALK              ..... :   2363.268 umol kg-1 <-> 0.3163082E+19 mol
 Ocean DOM_C            ..... :      1.229 umol kg-1 <-> 0.1645022E+16 mol
 Ocean DOM_C_13C        ..... :    -22.299 o/oo     
 Ocean DOM_C_14C        ..... :   -118.687 o/oo     
 Ocean DOM_P            ..... :      0.011 umol kg-1 <-> 0.1406002E+14 mol
 Ocean DOM_Fe           ..... :      0.000 umol kg-1 <-> 0.1241409E+11 mol
 Ocean FeL              ..... :      0.001 umol kg-1 <-> 0.8855806E+12 mol
 Ocean L                ..... :      0.000 umol kg-1 <-> 0.4529217E+12 mol
 Ocean Ca               ..... :  10250.506 umol kg-1 <-> 0.1371964E+20 mol
  
 --------------------------
 BULK OCEAN CARBONATE CHEMSITRY
  
 Carb chem conc_CO2         . :     27.545 umol kg-1 <-> 0.3686722E+17 mol
 Carb chem conc_CO3         . :     93.984 umol kg-1 <-> 0.1257909E+18 mol
 Carb chem conc_HCO3        . :   2121.249 umol kg-1 <-> 0.2839156E+19 mol
  
 ------------------------------
 SURFACE EXPORT PRODUCTION
  
 Export flux POC              :    256.883 umol cm-2 yr-1 <-> 0.9439860E+15 mol yr-1
 Export flux POC_13C          :    -25.212 o/oo     
 Export flux POC_14C          :   -137.039 o/oo     
 Export flux CaCO3            :     30.795 umol cm-2 yr-1 <-> 0.1131642E+15 mol yr-1
 Export flux CaCO3_13C        :      2.729 o/oo     
 Export flux CaCO3_14C        :    -81.689 o/oo     
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
  
 ------------------------------
 SEDIMENTATION
  
 Export flux POC              :     24.635 umol cm-2 yr-1 <-> 0.9052777E+14 mol yr-1
 Export flux POC_13C          :    -24.895 o/oo     
 Export flux POC_14C          :   -134.658 o/oo     
 Export flux CaCO3            :     20.143 umol cm-2 yr-1 <-> 0.7402176E+14 mol yr-1
 Export flux CaCO3_13C        :      2.726 o/oo     
 Export flux CaCO3_14C        :    -81.572 o/oo     
 Export flux det              :      4.517 umol cm-2 yr-1 <-> 0.1659771E+14 mol yr-1
 Export flux opal             :      3.161 umol cm-2 yr-1 <-> 0.1161650E+14 mol yr-1
 Export flux ash              :      0.000 umol cm-2 yr-1 <-> 0.0000000E+00 mol yr-1
  
 ------------------------------
 CARBON FLUX SUMMARY
  
 Total POC export   :   0.9439860E+15 mol yr-1 =  11.328 GtC yr-1
 Total CaCO3 export :   0.1131642E+15 mol yr-1 =   1.358 GtC yr-1
  
 Total POC rain     :   0.9052777E+14 mol yr-1 =   1.086 GtC yr-1
 Total CaCO3 rain   :   0.7402176E+14 mol yr-1 =   0.888 GtC yr-1
