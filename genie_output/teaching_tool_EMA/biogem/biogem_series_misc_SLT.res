 % time (yr) / mean (land) surface air temperature (degrees C)
       0.500    9.196351
       1.500    9.211512
       2.500    9.227219
       3.500    9.237132
       4.500    9.249800
       5.500    9.262280
       6.500    9.269202
       7.500    9.280548
       8.500    9.312830
       9.500    9.337351
      10.500    9.351137
      20.479    9.474016
      30.479    9.593156
      40.479    9.759760
      50.479   10.018033
      60.479   10.274978
      70.479   10.659644
      80.479   10.968400
      90.479   11.294374
