 % time (yr) / surface air temperature (degrees C)
       0.500   13.935
       1.500   13.951
       2.500   13.966
       3.500   13.975
       4.500   13.986
       5.500   13.996
       6.500   14.001
       7.500   14.011
       8.500   14.026
       9.500   14.038
      10.500   14.047
      20.479   14.142
      30.479   14.244
      40.479   14.378
      50.479   14.557
      60.479   14.745
      70.479   14.992
      80.479   15.227
      90.479   15.479
