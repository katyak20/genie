 % time (yr) / global rate (mol yr-1) / mean rate (mol kg-1 yr-1)
           0.500   0.688093E+20   0.225395E+01
           1.500   0.688766E+20   0.225615E+01
           2.500   0.689235E+20   0.225768E+01
           3.500   0.689593E+20   0.225886E+01
           4.500   0.689915E+20   0.225991E+01
           5.500   0.690207E+20   0.226087E+01
           6.500   0.690474E+20   0.226174E+01
           7.500   0.690740E+20   0.226261E+01
           8.500   0.690989E+20   0.226343E+01
           9.500   0.691219E+20   0.226418E+01
          10.500   0.691457E+20   0.226496E+01
          20.479   0.694154E+20   0.227380E+01
          30.479   0.697527E+20   0.228485E+01
          40.479   0.701720E+20   0.229858E+01
          50.479   0.706679E+20   0.231482E+01
          60.479   0.712278E+20   0.233316E+01
          70.479   0.718582E+20   0.235381E+01
          80.479   0.725918E+20   0.237784E+01
          90.479   0.734178E+20   0.240489E+01
