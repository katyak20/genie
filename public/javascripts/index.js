var socket;
//var socket;//=io.connect();
var months_count = 0;
var year_count = 0;
var landJsonURL = '/javascripts/ents.json';
var atmosphereJsonURL = '/javascripts/embm.json';
var oceanJsonURL = '/javascripts/goldstein.json';
var gsicJsonURL = '/javascripts/gsic.json';
var runFileRead;
var time_slices_div = document.getElementById('plot_container');
var form_params = {};
var sliderk7 = document.getElementById("k7Range");
var outputk7 = document.getElementsByName("k7")[0];
outputk7.value = sliderk7.value;
form_params.k7 = sliderk7.value;
sliderk7.addEventListener('change', function () {
    outputk7.value = this.value;
    form_params.k7 = this.value;
});
outputk7.addEventListener('change', function() {
    sliderk7.value = this.value;
    form_params.k7 = this.value;
});
var sliderk14 = document.getElementById("k14Range");
var outputk14 = document.getElementsByName("k14")[0];
outputk14.value = sliderk14.value;
form_params.k14 = sliderk14.value;
sliderk14.addEventListener('change', function () {
    outputk14.value = this.value;
    form_params.k14 = this.value;
});
outputk14.addEventListener('change', function() {
    sliderk14.value = this.value;
    form_params.k14 = this.value;
});
var sliderk17 = document.getElementById("k17Range");
var outputk17 = document.getElementsByName("k17")[0];
outputk17.value = sliderk17.value;
form_params.k17 = sliderk17.value;
sliderk17.addEventListener('change', function () {
    outputk17.value = this.value;
    form_params.k17 = this.value;
});
outputk17.addEventListener('change', function() {
    sliderk17.value = this.value;
    form_params.k17 = this.value;
});
var sliderk18 = document.getElementById("k18Range");
var outputk18 = document.getElementsByName("k18")[0];
outputk18.value = sliderk18.value;
form_params.k18 = sliderk18.value;
sliderk18.addEventListener('change', function () {
    outputk18.value = this.value;
    form_params.k18 = this.value;
});
outputk18.addEventListener('change', function() {
    sliderk18.value = this.value;
    form_params.k18 = this.value;
});
var sliderk20 = document.getElementById("k20Range");
var outputk20 = document.getElementsByName("k20")[0];
outputk20.value = sliderk20.value;
form_params.k20 = sliderk20.value;
sliderk20.addEventListener('change', function () {
    outputk20.value = this.value;
    form_params.k20 = this.value;
});
outputk20.addEventListener('change', function() {
    sliderk20.value = this.value;
    form_params.k20 = this.value;
});
var sliderk24 = document.getElementById("k24Range");
var outputk24 = document.getElementsByName("k24")[0];
outputk24.value = sliderk24.value;
form_params.k24 = sliderk24.value;
sliderk24.addEventListener('change', function () {
    outputk24.value = this.value;
    form_params.k24 = this.value;
});
outputk24.addEventListener('change', function() {
    sliderk24.value = this.value;
    form_params.k24 = this.value;
});
var sliderk26 = document.getElementById("k26Range");
var outputk26 = document.getElementsByName("k26")[0];
outputk26.value = sliderk26.value;
form_params.k26 = sliderk26.value;
sliderk26.addEventListener('change', function () {
    outputk26.value = this.value;
    form_params.k26 = this.value;
});
outputk26.addEventListener('change', function() {
    sliderk26.value = this.value;
    form_params.k26 = this.value;
});
var sliderk29 = document.getElementById("k29Range");
var outputk29 = document.getElementsByName("k29")[0];
outputk29.value = sliderk29.value;
form_params.k29 = sliderk29.value;
sliderk29.addEventListener('change', function () {
    outputk29.value = this.value;
    form_params.k29 = this.value;
});
outputk29.addEventListener('change', function() {
    sliderk29.value = this.value;
    form_params.k29 = this.value;
});
var sliderk32 = document.getElementById("k32Range");
var outputk32 = document.getElementsByName("k32")[0];
outputk32.value = sliderk32.value;
form_params.k32 = sliderk32.value;
sliderk32.addEventListener('change', function () {
    outputk32.value = this.value;
    form_params.k32 = this.value;
});
outputk32.addEventListener('change', function() {
    sliderk32.value = this.value;
    form_params.k32 = this.value;
});
var sliderkz0 = document.getElementById("kz0Range");
var outputkz0 = document.getElementsByName("kz0")[0];
outputkz0.value = sliderkz0.value;
form_params.kz = sliderkz0.value;
sliderkz0.addEventListener('change', function () {
    outputkz0.value = this.value;
    form_params.kz = this.value;
});
outputkz0.addEventListener('change', function() {
    sliderkz0.value = this.value;
    form_params.kz0 = this.value;
});
var slideremit0 = document.getElementById("emit0Range");
var outputemit0 = document.getElementsByName("emit0")[0];
outputemit0.value = slideremit0.value;
form_params.emit0 = slideremit0.value;
slideremit0.addEventListener('change', function () {
    outputemit0.value = this.value;
    form_params.emit0 = this.value;
});
outputemit0.addEventListener('change', function() {
    slideremit0.value = this.value;
    form_params.emit0 = this.value;
});
var slideremitG = document.getElementById("emitGRange");
var outputemitG = document.getElementsByName("emitG")[0];
outputemitG.value = slideremitG.value;
form_params.emit1 = slideremitG.value;
slideremitG.addEventListener('change', function () {
    outputemitG.value = this.value;
    form_params.emit1 = this.value;
});
outputemitG.addEventListener('change', function() {
    slideremitG.value = this.value;
    form_params.emitG = this.value;
});
var sliderolr_adj = document.getElementById("olr_adjRange");
var outputolr_adj = document.getElementsByName("olr_adj")[0];
outputolr_adj.value = sliderolr_adj.value;
form_params.olradj = sliderolr_adj.value;
sliderolr_adj.addEventListener('change', function () {
    outputolr_adj.value = this.value;
    form_params.olradj = this.value;
});
outputolr_adj.addEventListener('change', function() {
    sliderolr_adj.value = this.value;
    form_params.olr_adj = this.value;
});
var sliderdiffamp1 = document.getElementById("diffamp1Range");
var outputdiffamp1 = document.getElementsByName("diffamp1")[0];
outputdiffamp1.value = sliderdiffamp1.value;
form_params.diffamp1 = sliderdiffamp1.value;
sliderdiffamp1.addEventListener('change', function () {
    outputdiffamp1.value = this.value;
    form_params.diffamp1 = this.value;
});
outputdiffamp1.addEventListener('change', function() {
    sliderdiffamp1.value = this.value;
    form_params.diffamp1 = this.value;
});
var sliderdiffamp2 = document.getElementById("diffamp2Range");
var outputdiffamp2 = document.getElementsByName("diffamp2")[0];
outputdiffamp2.value = sliderdiffamp2.value;
form_params.diffamp2 = sliderdiffamp2.value;
sliderdiffamp2.addEventListener('change', function () {
    outputdiffamp2.value = this.value;
    form_params.diffamp2 = this.value;
});
outputdiffamp2.addEventListener('change', function() {
    sliderdiffamp2.value = this.value;
    form_params.diffamp2 = this.value;
});
var sliderdiffwid = document.getElementById("diffwidRange");
var outputdiffwid = document.getElementsByName("diffwid")[0];
outputdiffwid.value = sliderdiffwid.value;
form_params.diffwid = sliderdiffwid.value;
sliderdiffwid.addEventListener('change', function () {
    outputdiffwid.value = this.value;
    form_params.diffwid = this.value;
});
outputdiffwid.addEventListener('change', function() {
    sliderdiffwid.value = this.value;
    form_params.diffwid = this.value;
});
var sliderdiff1 = document.getElementById("diff1Range");
var outputdiff1 = document.getElementsByName("diff1")[0];
outputdiff1.value = sliderdiff1.value;
form_params.diff1 = sliderdiff1.value;
sliderdiff1.addEventListener('change', function () {
    outputdiff1.value = this.value;
    form_params.diff1 = this.value;
});
outputdiff1.addEventListener('change', function() {
    sliderdiff1.value = this.value;
    form_params.diff1 = this.value;
});
var sliderdiff2 = document.getElementById("diff2Range");
var outputdiff2 = document.getElementsByName("diff2")[0];
outputdiff2.value = sliderdiff2.value;
form_params.diff2 = sliderdiff2.value;
sliderdiff2.addEventListener('change', function () {
    outputdiff2.value = this.value;
    form_params.diff2 = this.value;
});
outputdiff2.addEventListener('change', function() {
    sliderdiff2.value = this.value;
    form_params.diff2 = this.value;
});

var slideradrag = document.getElementById("adragRange");
var outputadrag = document.getElementsByName("adrag")[0];
outputadrag.value = slideradrag.value;
form_params.adrag = slideradrag.value;
slideradrag.addEventListener('change', function () {
    outputadrag.value = this.value;
    form_params.adrag = this.value;
});
outputadrag.addEventListener('change', function() {
    slideradrag.value = this.value;
    form_params.adrag = this.value;
});

var sliderdiffsic = document.getElementById("diffsicRange");
var outputdiffsic = document.getElementsByName("diffsic")[0];
outputdiffsic.value = sliderdiffsic.value;
form_params.diffsic = sliderdiffsic.value;
sliderdiffsic.addEventListener('change', function () {
    outputdiffsic.value = this.value;
    form_params.diffsic = this.value;
});
outputdiffsic.addEventListener('change', function() {
    sliderdiffsic.value = this.value;
    form_params.diffsic = this.value;
});
var sliderscl_fwf = document.getElementById("scl_fwfRange");
var outputscl_fwf = document.getElementsByName("scl_fwf")[0];
outputscl_fwf.value = sliderscl_fwf.value;
form_params.scl_fwf = sliderscl_fwf.value;
sliderscl_fwf.addEventListener('change', function () {
    outputscl_fwf.value = this.value;
    form_params.scl_fwf = this.value;
});
outputscl_fwf.addEventListener('change', function() {
    sliderscl_fwf.value = this.value;
    form_params.scl_fwf = this.value;
});

var monthly_averages = {
    'time': [],
    'GAT': [],
    'NHAT': [],
    'SHAT': [],
    'NAMLT': [],
    'NAELT': [],
    'SAMLT': [],
    'SAALT': [],
    'GVC': [],
    'NAMVC': [],
    'NAEVC': [],
    'SAMVC': [],
    'SAAVC': [],
    'GSC': [],
    'NAMSC': [],
    'NAESC': [],
    'SAMSC': [],
    'SAASC': [],
    'ATMC': [],
    'ATMC02': [],
    'OCNC': [],
    'months': []
};

var annual_averages = {
    'time': [],
    'GAT': [],
    'NHAT': [],
    'SHAT': [],
    'NAMLT': [],
    'NAELT': [],
    'SAMLT': [],
    'SAALT': [],
    'GVC': [],
    'NAMVC': [],
    'NAEVC': [],
    'SAMVC': [],
    'SAAVC': [],
    'GSC': [],
    'NAMSC': [],
    'NAESC': [],
    'SAMSC': [],
    'SAASC': [],
    'ATMC': [],
    'ATMC02': [],
    'OCNC': [],
    'months': []
};

function UpdateTablesWithAnnualAverages(values) {
    var annual_table = document.getElementById('annual_averages_table');
    var printable_table_1 = document.getElementById('printable_values_table_1');
    var printable_table_2 = document.getElementById('printable_values_table_2');
    var tr = annual_table.insertRow();
    var tr_printable_1 = printable_table_1.insertRow();
    var tr_printable_2 = printable_table_2.insertRow();
    for (var i = 0; i < values.length; i++) {
        var tabCell = tr.insertCell(-1);
        tabCell.innerHTML = values[i];
        if (i == 0) {
            var tabCellPrintable_1 = tr_printable_1.insertCell(-1);
            tabCellPrintable_1.innerHTML = values[i];

            var tabCellPrintable_2 = tr_printable_2.insertCell(-1);
            tabCellPrintable_2.innerHTML = values[i];
        }
        else if (i < 11) {
            var tabCellPrintable_1 = tr_printable_1.insertCell(-1);
            tabCellPrintable_1.innerHTML = values[i];
        }
        else {
            var tabCellPrintable_2 = tr_printable_2.insertCell(-1);
            tabCellPrintable_2.innerHTML = values[i];
        }

    }
}

function loadJSON(url) {
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        req.overrideMimeType("application/json");
        req.open('GET', url, true);
        req.onreadystatechange = function (evt) {
            if (req.readyState == 4) {
                if (req.status == 200) {

                    try {
                        var netcdf_data_for_simulation = JSON.parse(req.responseText);
                        resolve(netcdf_data_for_simulation);
                        var simulation_param = document.getElementById("typeOfData").value;
                        var param_type = simulation_param.split(".");

                    }
                    catch (ex) {
                        reject(ex);
                    }
                }
                else {
                    reject(req.status);
                }
            }
        };
        req.send(null);
    });
}

var container = document.getElementById("s397_container");
var Genie_Frontend = new genie_frontend(container, data.netcdf_options, netcdf_data, netcdf_data_0, data.heatmap_options, data.decode_average_titles, data.decode_heatmap_params, 36);

Genie_Frontend.init();

document.getElementById("run").addEventListener('click', function () {
    var tb = document.getElementById("annual_averages_table");
    while (tb.rows.length > 1) {
        tb.deleteRow(1);
    }
    ;
    var annual_slice_function_call_counter = 0;
    var monthly_slice_function_call_counter = 0;

    //  socket = io.connect(SERVER_IP, {'forceNew': true});
    console.log("Form params" + JSON.stringify(form_params));
    socket = io();
    socket.emit('clicked', form_params);
    console.log(JSON.stringify(form_params));
    this.setAttribute('disabled', 'true');
    document.getElementById('reset_button').disabled = true;
    var count = 0;
    runFileRead = setInterval(function(){
            var jsonURL;
            count++;
            console.log(count);
            var simulation_param = document.getElementById("typeOfData").value;
            var param_type = simulation_param.split(".");
            console.log("From run-> click : " + param_type[0] + "   " + param_type[1]);

            switch (param_type[0]) {
                case 'ents':
                    jsonURL = landJsonURL;
                    break;
                case 'embm':
                    jsonURL = atmosphereJsonURL;
                    break;
                case 'gold':
                    jsonURL = oceanJsonURL;
                    break;
                case 'gsic':
                    jsonURL = gsicJsonURL;
                    break;
                default:
                    jsonURL = landJsonURL;
                    break;
            }
            console.log(jsonURL);
            loadJSON(jsonURL).then(
                function (json_data) {
                    //console.log(json_data);
                    var simulation_param = document.getElementById("typeOfData").value;
                    var param_type = simulation_param.split(".");

                    Genie_Frontend.updateGraph('plot_container', param_type[1], json_data, data.heatmap_options[param_type[1]], false);
                    document.getElementById('stop').disabled = false;
                },
                function (error) {
                    console.log(error);
                }
            );
        }
        , 500);
    // });
    socket.on('genie_process_finished', function (socket_data) {
            monthly_slice_function_call_counter++;
            console.log("Monthly slice_change counter value  " + monthly_slice_function_call_counter);
            clearInterval(runFileRead);
            console.log('Genie_process finished clearing interval RUNFILEREAD....' + socket.id);
            document.getElementById('run').disabled = false;

        }
    )

    socket.on('timeslice_changed', function (socket_data) {
            monthly_slice_function_call_counter++;
            console.log("Monthly slice_change counter value  " + monthly_slice_function_call_counter);
            var select_element = document.getElementById('typeOfLineData');
            var selected_value = select_element.value;
            var selected_value_detailed = select_element.options[select_element.selectedIndex].text;

            var linegraphDiv = document.getElementById('lineseries_div');
            months_count++;

            var timeslice_data_array = socket_data.split("  ");

            // console.log('Socket data ----Time_slice changed : ' + timeslice_data_array);


            monthly_averages.time.push(Number(timeslice_data_array[1]));
            monthly_averages.GAT.push(Number(timeslice_data_array[2]));

            monthly_averages.NHAT.push(Number(timeslice_data_array[3]));

            monthly_averages.SHAT.push(Number(timeslice_data_array[4]));

            monthly_averages.NAMLT.push(Number(timeslice_data_array[5]));

            monthly_averages.NAELT.push(Number(timeslice_data_array[6]));

            monthly_averages.SAMLT.push(Number(timeslice_data_array[7]));

            monthly_averages.SAALT.push(Number(timeslice_data_array[8]));

            monthly_averages.GVC.push(Number(timeslice_data_array[9]));

            monthly_averages.NAMVC.push(Number(timeslice_data_array[10]));

            monthly_averages.NAEVC.push(Number(timeslice_data_array[11]));

            monthly_averages.SAMVC.push(Number(timeslice_data_array[12]));

            monthly_averages.SAAVC.push(Number(timeslice_data_array[13]));

            monthly_averages.GSC.push(Number(timeslice_data_array[14]));

            monthly_averages.NAMSC.push(Number(timeslice_data_array[15]));

            monthly_averages.NAESC.push(Number(timeslice_data_array[16]));

            monthly_averages.SAMSC.push(Number(timeslice_data_array[17]));

            monthly_averages.SAASC.push(Number(timeslice_data_array[18]));

            monthly_averages.ATMC.push(Number(timeslice_data_array[19]));

            monthly_averages.ATMC02.push(Number(timeslice_data_array[20]));

            monthly_averages.OCNC.push(Number(timeslice_data_array[21]));

            monthly_averages.months.push(months_count / 24);

            var chart = Highcharts.charts[linegraphDiv.getAttribute('data-highcharts-chart')];
            chart.series[0].setData(monthly_averages[selected_value]);
            chart.series[0].setName(selected_value_detailed);

        }
    )

    socket.on('annual_slice_changed', function (socket_data) {
            annual_slice_function_call_counter++;
            console.log("Annual_slice_change counter value  " + annual_slice_function_call_counter);
            var select_element = document.getElementById('typeOfLineData');
            var selected_value = select_element.value;
            var selected_value_detailed = select_element.options[select_element.selectedIndex].text;

            var annual_linegraphDiv = document.getElementById('annual_lineseries_div');
            year_count++;
            var annual_slice_data_array = socket_data.substring(2).split("  ").map(Number);

            annual_averages.time.push(Number(annual_slice_data_array[0]));
            annual_averages.GAT.push(Number(annual_slice_data_array[1]));
            annual_averages.NHAT.push(Number(annual_slice_data_array[2]));
            annual_averages.SHAT.push(Number(annual_slice_data_array[3]));
            annual_averages.NAMLT.push(Number(annual_slice_data_array[4]));
            annual_averages.NAELT.push(Number(annual_slice_data_array[5]));
            annual_averages.SAMLT.push(Number(annual_slice_data_array[6]));
            annual_averages.SAALT.push(Number(annual_slice_data_array[7]));
            annual_averages.GVC.push(Number(annual_slice_data_array[8]));
            annual_averages.NAMVC.push(Number(annual_slice_data_array[9]));
            annual_averages.NAEVC.push(Number(annual_slice_data_array[10]));
            annual_averages.SAMVC.push(Number(annual_slice_data_array[11]));
            annual_averages.SAAVC.push(Number(annual_slice_data_array[12]));
            annual_averages.GSC.push(Number(annual_slice_data_array[13]));
            annual_averages.NAMSC.push(Number(annual_slice_data_array[14]));
            annual_averages.NAESC.push(Number(annual_slice_data_array[15]));
            annual_averages.SAMSC.push(Number(annual_slice_data_array[16]));
            annual_averages.SAASC.push(Number(annual_slice_data_array[17]));
            annual_averages.ATMC.push(Number(annual_slice_data_array[18]));
            annual_averages.ATMC02.push(Number(annual_slice_data_array[19]));
            annual_averages.OCNC.push(Number(annual_slice_data_array[20]));


            var chart = Highcharts.charts[annual_linegraphDiv.getAttribute('data-highcharts-chart')];
            chart.series[0].setData(annual_averages[selected_value]);
            chart.series[0].setName(selected_value_detailed);

            UpdateTablesWithAnnualAverages(annual_slice_data_array);
        }
    )

    socket.on('ncdump-json_output', function (socket_data) {

        console.log(socket_data);

    })
});


document.getElementById("download").addEventListener('click', function () {
    // window.print();
    var doc = new jsPDF('l', 'pt');

    doc.setFontSize(16);
    doc.setTextColor(40);
    doc.setFontStyle('normal');
    doc.text("Annual averaged values (for 100 years)", 45, 40);
    doc.setFontSize(10);
    doc.setTextColor(30);
    doc.setFontStyle('normal');
    doc.text("diff1: " + form_params.diff1 + " diff2: " + form_params.diff2 + " adrag: " + form_params.adrag + " diffsic: " + form_params.diffsic + " diffamp(1): " + form_params.diffamp1 + " diffamp(2): " + form_params.diffamp2  + " diffwid: " + form_params.diffwid + " scl_fwf: " + form_params.scl_fwf + " olr_adj: " + form_params.olradj, 45, 58);
    doc.text(" k7: " + form_params.k7 + " k14: " + form_params.k14 + " k17: " + form_params.k17 + " k18: " + form_params.k18 + " k20: " + form_params.k20 + " k24: " + form_params.k24 + " k26: " + form_params.k26 + " k29: " + form_params.k29 + " k32: " + form_params.k32 +" kz: " + form_params.kz + " emit0: " + form_params.emit0 +" emitG: " + form_params.emit1, 45, 72);

    var res = doc.autoTableHtmlToJson(document.getElementById("printable_values_table_1"));
    doc.autoTable(res.columns, res.data, {
        styles: {
            overflow: 'linebreak',
            fontSize: 10,
            tableWidth: 'auto',
            columnWidth: 'auto',
        },
        columnStyles: {
            0: {columnWidth: 40}
        },
        margin: {top: 80}
    });

    var options = {
        styles: {
            overflow: 'linebreak',
            fontSize: 10,
            tableWidth: 'auto',
            columnWidth: 'auto',
        },
        margin: {
            top: 50
        },
        columnStyles: {
            0: {columnWidth: 40}
        },
        startY: doc.autoTableEndPosY() + 20
    };

    var res = doc.autoTableHtmlToJson(document.getElementById("printable_values_table_2"));
    doc.autoTable(res.columns, res.data, options);

    doc.save("table.pdf");

});

document.getElementById("stop").addEventListener('click', function () {
    clearInterval(runFileRead);
    console.log("Socket ID" + socket.id);
    socket.emit('stop');
    document.getElementById('run').disabled = false;
    document.getElementById('reset_button').disabled = false;
    monthly_averages = {
        'time': [],
        'GAT': [],
        'NHAT': [],
        'SHAT': [],
        'NAMLT': [],
        'NAELT': [],
        'SAMLT': [],
        'SAALT': [],
        'GVC': [],
        'NAMVC': [],
        'NAEVC': [],
        'SAMVC': [],
        'SAAVC': [],
        'GSC': [],
        'NAMSC': [],
        'NAESC': [],
        'SAMSC': [],
        'SAASC': [],
        'ATMC': [],
        'ATMC02': [],
        'OCNC': [],
        'months': []
    };
    annual_averages = {
        'time': [],
        'GAT': [],
        'NHAT': [],
        'SHAT': [],
        'NAMLT': [],
        'NAELT': [],
        'SAMLT': [],
        'SAALT': [],
        'GVC': [],
        'NAMVC': [],
        'NAEVC': [],
        'SAMVC': [],
        'SAAVC': [],
        'GSC': [],
        'NAMSC': [],
        'NAESC': [],
        'SAMSC': [],
        'SAASC': [],
        'ATMC': [],
        'ATMC02': [],
        'OCNC': [],
        'months': []
    };
    socket.disconnect();
    socket.close();


});

document.getElementById("paramsForm").addEventListener('reset', function (e) {
    setTimeout(function() {
       /* var sliderdiffwid = document.getElementById("diffwidRange");
        form_params.diffwid =sliderdiffwid.value;*/
        var sliderk7 = document.getElementById("k7Range");
        form_params.k7 = sliderk7.value;

        var sliderk14 = document.getElementById("k14Range");
        form_params.k14 = sliderk14.value;


        var sliderk17 = document.getElementById("k17Range");
        form_params.k17 = sliderk17.value;

        var sliderk18 = document.getElementById("k18Range");
        form_params.k18 = sliderk18.value;

        var sliderk20 = document.getElementById("k20Range");
        form_params.k20 = sliderk20.value;

        var sliderk24 = document.getElementById("k24Range");
        form_params.k24 = sliderk24.value;

        var sliderk26 = document.getElementById("k26Range");
        form_params.k26 = sliderk26.value;

        var sliderk29 = document.getElementById("k29Range");
        form_params.k29 = sliderk29.value;

        var sliderk32 = document.getElementById("k32Range");
        form_params.k32 = sliderk32.value;

        var sliderkz0 = document.getElementById("kz0Range");
        form_params.kz = sliderkz0.value;

        var slideremit0 = document.getElementById("emit0Range");
        form_params.emit0 = slideremit0.value;

        var slideremitG = document.getElementById("emitGRange");
        form_params.emit1 = slideremitG.value;

        var sliderolr_adj = document.getElementById("olr_adjRange");
        form_params.olradj = sliderolr_adj.value;

        var sliderdiffamp1 = document.getElementById("diffamp1Range");
        form_params.diffamp1 = sliderdiffamp1.value;

        var sliderdiffamp2 = document.getElementById("diffamp2Range");
        form_params.diffamp2 = sliderdiffamp2.value;

        var sliderdiffwid = document.getElementById("diffwidRange");
        form_params.diffwid = sliderdiffwid.value;

        var sliderdiff1 = document.getElementById("diff1Range");
        form_params.diff1 = sliderdiff1.value;

        var sliderdiff2 = document.getElementById("diff2Range");
        form_params.diff2 = sliderdiff2.value;


        var slideradrag = document.getElementById("adragRange");
        form_params.adrag = slideradrag.value;

        var sliderdiffsic = document.getElementById("diffsicRange");
        form_params.diffsic = sliderdiffsic.value;

        var sliderscl_fwf = document.getElementById("scl_fwfRange");
        form_params.scl_fwf = sliderscl_fwf.value;
        console.log("From reset listener" + JSON.stringify(form_params));
    }, 400);
});

var layerSlider = document.getElementById("layerSlider");
var layerSpan = document.getElementById("layerSpan");
layerSpan.innerHTML = "Ocean layer: " + layerSlider.value;
layerSlider.addEventListener('change', function () {
    layerSpan.innerHTML = "Ocean layer: " + this.value;
});

document.getElementById("typeOfLineData").addEventListener('change', function () {
    var linegraphDiv = document.getElementById('lineseries_div');
    var annual_linegraphDiv = document.getElementById('annual_lineseries_div');

    var chart = Highcharts.charts[linegraphDiv.getAttribute('data-highcharts-chart')];
    chart.series[0].setData(monthly_averages[this.value]);
    chart.series[0].setName(this.value);

    var chart = Highcharts.charts[annual_linegraphDiv.getAttribute('data-highcharts-chart')];
    chart.series[0].setData(annual_averages[this.value]);
    chart.series[0].setName(selected_value_detailed);
});
