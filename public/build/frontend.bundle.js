'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var genie_frontend = function () {
    function genie_frontend(container, options_array, netcdf_data, netcdf_data_0, heatmap_options, decode_average_titles, decode_heatmap_params, gridsize) {
        _classCallCheck(this, genie_frontend);

        this.container = container;
        this.dom = {

            buttonsContainer: document.createElement('div'),
            second_row: document.getElementById('second_row'),
            third_row: document.getElementById('third_row'),
            forth_row: document.getElementById('forth_row'),
            fifth_row: document.getElementById('fifth_row'),
            sixth_row: document.getElementById('sixth_row'),
            plot_container: document.createElement('div'),
            line_graphs1_container: document.createElement('div'),
            lineseries_container: document.createElement('div'),
            annual_averages_plot_container: document.createElement('div'),
            line_graphs2_container: document.createElement('div'),
            annual_lineseries_container: document.createElement('div'),
            descriptionParagraphForSelect: document.createElement('p'),
            selectElement: document.createElement('select'),
            layerSpan: document.createElement('span'),
            minSpan: document.createElement('span'),
            maxSpan: document.createElement('span'),
            layerSlider: document.createElement('input'),
            dataList: document.createElement('datalist'),
            selectElement_for_LineGraphs: document.createElement('select'),
            options: new Array(),
            options_forLineChart: new Array(),
            header: {
                header_container: document.createElement('div'),
                header_h2: document.createElement('h2')

            },
            controls: {
                container: document.getElementById('buttonControls'),
                run: document.createElement('button'),
                stop: document.createElement('button'),
                reset: document.createElement('button'),
                download: document.createElement('button'),
                downloadMonthlySeries: document.createElement('a'),
                downloadAnnualData: document.createElement('a')
            },
            sliders: {
                container: document.createElement('div'),
                k7: document.createElement('input'),
                k17: document.createElement('input'),
                k18: document.createElement('input')
            },
            seasonal_values_table_1: document.createElement('table'),
            seasonal_values_table_2: document.createElement('table'),
            annual_averages_table: document.createElement('table')

        };
        this.gridSize = gridsize;
        this.netcdf_options = options_array;
        this.netcdf_data = netcdf_data;
        this.netcdf_data_0 = netcdf_data_0;
        this.decode_average_titles = decode_average_titles;
        this.decode_heatmap_params = decode_heatmap_params;
        this.heatmap_options = heatmap_options;
        this.numberOfLayersInOceanModel = 16;
        this.anomalyMinColor = 'rgba(255,0,0,0.3)';
        this.anomalyMaxColor = 'rgba(0,0,255,0.3)';
        this.global_data_min = 0;
        this.global_data_max = 0;
        this.global_data_anomaly_min = 0;
        this.global_data_anomaly_max = 0;
    }

    _createClass(genie_frontend, [{
        key: 'init',
        value: function init() {
            this.container.setAttribute('class', 'container');
            this.container.setAttribute('aria-describedby', this.container.id + '_description');
            this.container.setAttribute('role', 'application');
            this.dom.header.header_h2.textContent = "Genie earth system simulator";

            this.dom.selectElement.id = "typeOfData";
            this.dom.selectElement.setAttribute('tabindex', '0');
            this.dom.controls.container.appendChild(this.dom.selectElement);
            // this.dom.options=this.netcdf_options.dropdown_text;
            for (var i = 0; i < this.netcdf_options.dropdown_text.length; i++) {

                this.dom.options[i] = document.createElement("option");
                this.dom.options[i].value = this.netcdf_options.dropdown_value[i];
                this.dom.options[i].text = this.netcdf_options.dropdown_text[i];
                this.dom.selectElement.appendChild(this.dom.options[i]);
            }

            this.dom.selectElement.addEventListener('change', function () {
                var selected_param = this.value.toString().split(".");
                switch (selected_param[1]) {
                    case 'fv':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'fv', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['fv'], true);
                        //    Genie_Frontend.drawGraph('plot_container_annual_averages', 'fv', Genie_Frontend.getNetcdfData());
                        break;
                    case 'land_temp':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'land_temp', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['land_temp'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'land_temp', Genie_Frontend.getNetcdfData());
                        break;
                    case 'land_water':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'land_water', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['land_water'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'land_water', Genie_Frontend.getNetcdfData());
                        break;
                    case 'leaf':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'leaf', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['leaf'], true);
                        //   Genie_Frontend.drawGraph('plot_container_annual_averages', 'leaf', Genie_Frontend.getNetcdfData());
                        break;
                    case 'photosynthesis':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'photosynthesis', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['photosynthesis'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'photosynthesis', Genie_Frontend.getNetcdfData());
                        break;
                    case 'snow':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'snow', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['snow'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'snow', Genie_Frontend.getNetcdfData());
                        break;
                    case 'soil_resp':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'soil_resp', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['soil_resp'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'soil_resp', Genie_Frontend.getNetcdfData());
                        break;
                    case 'soilC':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'soilC', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['soilC'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'soilC', Genie_Frontend.getNetcdfData());
                        break;
                    case 'veg_resp':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'veg_resp', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['veg_resp'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'veg_resp', Genie_Frontend.getNetcdfData());
                        break;
                    case 'vegC':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'vegC', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['vegC'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'vegC', Genie_Frontend.getNetcdfData());
                        break;
                    case 'air_temp':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'air_temp', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['air_temp'], true);
                        // Genie_Frontend.drawGraph('plot_container_annual_averages', 'air_temp', Genie_Frontend.getNetcdfData());
                        break;
                    case 'humidity':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'humidity', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['humidity'], true);
                        //    Genie_Frontend.drawGraph('plot_container_annual_averages', 'humidity', Genie_Frontend.getNetcdfData());
                        break;
                    case 'pptn':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'pptn', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['pptn'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'pptn', Genie_Frontend.getNetcdfData());
                        break;
                    case 'evap':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'evap', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['evap'], true);
                        // Genie_Frontend.drawGraph('plot_container_annual_averages', 'evap', Genie_Frontend.getNetcdfData());
                        break;
                    case 'temp':
                        Genie_Frontend.dom.layerSpan.style.visibility = "visible";
                        Genie_Frontend.dom.layerSlider.style.visibility = "visible";
                        Genie_Frontend.updateGraph('plot_container', 'temp', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['temp'], true);
                        //   Genie_Frontend.drawGraph('plot_container_annual_averages', 'temp', Genie_Frontend.getNetcdfData());
                        break;
                    case 'density':
                        Genie_Frontend.dom.layerSpan.style.visibility = "visible";
                        Genie_Frontend.dom.layerSlider.style.visibility = "visible";
                        Genie_Frontend.updateGraph('plot_container', 'density', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['density'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'density', Genie_Frontend.getNetcdfData());
                        break;
                    case 'salinity':
                        Genie_Frontend.dom.layerSpan.style.visibility = "visible";
                        Genie_Frontend.dom.layerSlider.style.visibility = "visible";
                        Genie_Frontend.updateGraph('plot_container', 'salinity', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['salinity'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'salinity', Genie_Frontend.getNetcdfData());
                        break;
                    case 'bathymerty':
                        Genie_Frontend.dom.layerSpan.style.visibility = "visible";
                        Genie_Frontend.dom.layerSlider.style.visibility = "visible";
                        Genie_Frontend.updateGraph('plot_container', 'bathymetry', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['bathymetry'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'bathymetry', Genie_Frontend.getNetcdfData());
                        break;
                    case 'sic_temp':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'sic_temp', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['sic_temp'], true);
                        // Genie_Frontend.drawGraph('plot_container_annual_averages', 'sic_temp', Genie_Frontend.getNetcdfData());
                        break;
                    case 'sic_cover':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'sic_cover', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['sic_cover'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'sic_cover', Genie_Frontend.getNetcdfData());
                        break;
                    case 'sic_height':
                        Genie_Frontend.dom.layerSpan.style.visibility = "hidden";
                        Genie_Frontend.dom.layerSlider.style.visibility = "hidden";
                        Genie_Frontend.updateGraph('plot_container', 'sic_height', Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options['sic_height'], true);
                        //  Genie_Frontend.drawGraph('plot_container_annual_averages', 'sic_height', Genie_Frontend.getNetcdfData());
                        break;

                }
            }, false);

            this._makeSelectElementForLineGraphs();
            this.dom.selectElement_for_LineGraphs.selectedIndex = 0;

            this.dom.layerSpan.id = "layerSpan";
            this.dom.layerSpan.style.visibility = "hidden";
            this.dom.controls.container.appendChild(this.dom.layerSpan);

            this.dom.layerSlider.id = "layerSlider";
            this.dom.layerSlider.setAttribute('type', 'range');
            this.dom.layerSlider.setAttribute('min', '1');
            this.dom.layerSlider.setAttribute('max', '16');
            this.dom.layerSlider.setAttribute('step', '1');
            this.dom.layerSlider.setAttribute('class', 'slider');
            this.dom.layerSlider.setAttribute('list', 'ticks');
            this.dom.layerSlider.value = 1;
            this.dom.layerSlider.style.visibility = "hidden";

            this.dom.dataList.id = 'ticks';
            for (var i = 1; i <= this.numberOfLayersInOceanModel; i = i + 2) {
                this.dom.options[i] = document.createElement("option");
                this.dom.options[i].value = i;
                this.dom.dataList.appendChild(this.dom.options[i]);
            }

            this.dom.layerSlider.addEventListener('change', function () {
                var selectElement_selectedValue = document.getElementById('typeOfData').value.toString().split(".");
                Genie_Frontend.updateGraph('plot_container', selectElement_selectedValue[1], Genie_Frontend.getNetcdfData(), Genie_Frontend.heatmap_options[selectElement_selectedValue[1]], true);
            });

            this.dom.controls.container.appendChild(this.dom.layerSlider);
            this.dom.controls.container.appendChild(this.dom.dataList);
            this.dom.layerSpan.innerHTML = "Ocean layer: " + this.dom.layerSlider.value;

            this._makeControls();
            // this.container.appendChild(this.dom.second_row);
            this.dom.plot_container.id = "plot_container";
            this.dom.second_row.appendChild(this.dom.plot_container);
            this.dom.line_graphs1_container.className = 'line_graphs_container';
            this.dom.lineseries_container.id = 'lineseries_div';
            this.dom.second_row.appendChild(this.dom.line_graphs1_container);
            this.dom.line_graphs1_container.appendChild(this.dom.lineseries_container);
            this.dom.annual_averages_plot_container.id = "plot_container_anomaly_data";
            this.dom.third_row.appendChild(this.dom.annual_averages_plot_container);
            this.dom.line_graphs2_container.className = 'line_graphs_container';
            this.dom.annual_lineseries_container.id = 'annual_lineseries_div';
            this.dom.third_row.appendChild(this.dom.line_graphs2_container);
            this.dom.line_graphs2_container.appendChild(this.dom.annual_lineseries_container);

            this.drawGraph('plot_container', 'fv', this.netcdf_data, this.heatmap_options['fv']);
            this.drawGraph('plot_container_anomaly_data', 'fv', this.netcdf_data_0, this.heatmap_options['fv']);
            this.drawMonthlyLinechart('', '');
            this.drawAnnualLinechart('', '');
            this.createValuesTables();
        }
    }, {
        key: '_makeControls',
        value: function _makeControls() {
            this._makeRunButton();
            this._makeStopButton();
            this._makeResetButton();
            this._makeDownloadButton();
            this._makeDownloadMonthlySeriesButton();
            this._makeDownloadAnnualDataButton();

            this.dom.controls.container.appendChild(this.dom.controls.run);
            this.dom.controls.container.appendChild(this.dom.controls.stop);
            this.dom.controls.container.appendChild(this.dom.controls.reset);
            this.dom.controls.container.appendChild(this.dom.controls.download);
            this.dom.controls.container.appendChild(this.dom.controls.downloadMonthlySeries);
            this.dom.controls.container.appendChild(this.dom.controls.downloadAnnualData);
        }
    }, {
        key: '_makeRunButton',
        value: function _makeRunButton() {
            this.dom.controls.run.setAttribute('class', 'run');
            this.dom.controls.run.setAttribute('tabindex', '0');
            this.dom.controls.run.innerHTML = 'Start';
            this.dom.controls.run.className += ' tabs_controls vle';
            this.dom.controls.run.id = "run";
            //    this.dom.controls.run.disabled ='false';
            this.dom.controls.run.addEventListener('click', function () {});
        }
    }, {
        key: '_makeStopButton',
        value: function _makeStopButton() {
            this.dom.controls.stop.setAttribute('class', 'stop');
            this.dom.controls.stop.setAttribute('tabindex', '0');
            this.dom.controls.stop.innerHTML = 'Stop';
            this.dom.controls.stop.className += ' tabs_controls vle';
            this.dom.controls.stop.id = "stop";
            //  this.dom.controls.run.disabled ='false';
            //  this.dom.controls.stop.disabled='true';
            this.dom.controls.stop.addEventListener('click', function () {});
        }
    }, {
        key: '_makeResetButton',
        value: function _makeResetButton() {
            this.dom.controls.reset.setAttribute('class', 'reset_button');
            this.dom.controls.reset.setAttribute('tabindex', '0');
            this.dom.controls.reset.type = "reset";
            this.dom.controls.reset.innerHTML = 'Reset';
            this.dom.controls.reset.value = "Set Default values";
            this.dom.controls.reset.className += ' tabs_controls vle';
            this.dom.controls.reset.id = 'reset_button';
            /* this.dom.controls.reset.addEventListener('click', () => {
                 document.document.getElementsByTagName("form")[0].reset();
             });*/
        }
    }, {
        key: '_makeDownloadButton',
        value: function _makeDownloadButton() {
            this.dom.controls.download.setAttribute('class', 'download');
            this.dom.controls.download.setAttribute('tabindex', '0');
            this.dom.controls.download.innerHTML = 'PDF';
            this.dom.controls.download.className += ' tabs_controls vle';
            this.dom.controls.download.id = 'download';
            this.dom.controls.stop.disabled = 'true';
            this.dom.controls.download.addEventListener('click', function () {});
        }
    }, {
        key: '_makeDownloadMonthlySeriesButton',
        value: function _makeDownloadMonthlySeriesButton() {
            this.dom.controls.downloadMonthlySeries.href = "time_series.txt";
            this.dom.controls.downloadMonthlySeries.download = 'time_series.txt';
            this.dom.controls.downloadMonthlySeries.target = "_blank";
            this.dom.controls.downloadMonthlySeries.textContent = 'Monthly series';
            this.dom.controls.downloadMonthlySeries.setAttribute('class', 'a_vle');
        }
    }, {
        key: '_makeDownloadAnnualDataButton',
        value: function _makeDownloadAnnualDataButton() {
            this.dom.controls.downloadAnnualData.textContent = 'Annual data';
            this.dom.controls.downloadAnnualData.href = 'ann_series.txt';
            this.dom.controls.downloadAnnualData.download = 'ann_series.txt';
            this.dom.controls.downloadAnnualData.target = "_blank";
            this.dom.controls.downloadAnnualData.setAttribute('class', 'a_vle');
        }
    }, {
        key: 'getNetcdfData',
        value: function getNetcdfData() {
            return this.netcdf_data;
        }
    }, {
        key: '_makeSelectElementForLineGraphs',
        value: function _makeSelectElementForLineGraphs() {
            this.dom.selectElement_for_LineGraphs.id = "typeOfLineData";
            this.dom.selectElement_for_LineGraphs.setAttribute('tabindex', '0');
            this.dom.controls.container.appendChild(this.dom.selectElement_for_LineGraphs);
            var i = 1;
            for (var x in this.decode_average_titles) {
                this.dom.options_forLineChart[i] = document.createElement("option");
                this.dom.options_forLineChart[i].value = x;
                this.dom.options_forLineChart[i].text = this.decode_average_titles[x];
                this.dom.selectElement_for_LineGraphs.appendChild(this.dom.options_forLineChart[i]);
                i++;
            }
            this.dom.selectElement_for_LineGraphs.addEventListener('change', function () {
                var title = this.options[this.selectedIndex].text; //Genie_Frontend.getAverageTitle('GAT');
                switch (this.value) {
                    case 'GAT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);

                        break;
                    case 'NHAT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SHAT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'NAMLT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'NAELT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SAMLT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SAALT':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'GVC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'NAMVC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'NAEVC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SAMVC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SAAVC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'GSC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'NAMSC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'NAESC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SAMSC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'SAASC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'ATMC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'ATMC02':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                    case 'OCNC':
                        Genie_Frontend.drawMonthlyLinechart(title, this.options[this.selectedIndex].text);
                        Genie_Frontend.drawAnnualLinechart(title, this.options[this.selectedIndex].text);
                        break;
                }
            }, false);
        }
    }, {
        key: 'getAverageTitle',
        value: function getAverageTitle(abriviation) {
            return this.decode_average_titles[abriviation];
        }
    }, {
        key: 'updateGraph',
        value: function updateGraph(container, selectParam, netcdf_data, heatmap_options_for_selected_param, has_select_param_changed) {
            if (has_select_param_changed === true) {
                this.global_data_min = 0;
                this.global_data_max = 0;
                this.global_data_anomaly_min = 0;
                this.global_data_anomaly_max = 0;
            };
            var months_of_the_year = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var year_being_plotted = netcdf_data['year'];
            var month = parseInt(netcdf_data['month']);
            var year_being_plotted_string = "<b>0</b>" + '<i>-st</i> year';;
            if (year_being_plotted == 1) {
                year_being_plotted_string = '<b>' + year_being_plotted + '</b>' + '<i>-st</i> year';
            } else if (year_being_plotted == 2) {
                year_being_plotted_string = '<b>' + year_being_plotted + '</b>' + '<i>-nd</i> year';
            } else if (year_being_plotted == 3) {
                year_being_plotted_string = '<b>' + year_being_plotted + '</b>' + '<i>-d</i> year';
            } else {
                year_being_plotted_string = '<b>' + year_being_plotted + '</b>' + '<i>-th</i> year';
            }
            var selected_data = netcdf_data[selectParam];
            var selected_data_0 = this.netcdf_data_0[selectParam];

            var heatmap_label = this.heatmap_options[selectParam].description;
            if (['temp', 'salinity', 'density', 'bathymetry'].indexOf(selectParam) > -1) {
                var ocean_depth_layer = document.getElementById("layerSlider").value;
                switch (ocean_depth_layer) {
                    case "1":
                        selected_data = selected_data.slice(0, 1296);
                        selected_data_0 = selected_data_0.slice(0, 1296);
                        break;
                    case "2":
                        selected_data = selected_data.slice(1296, 2592);
                        selected_data_0 = selected_data_0.slice(1296, 2592);

                        break;
                    case "3":
                        selected_data = selected_data.slice(2592, 3888);
                        selected_data_0 = selected_data_0.slice(2592, 3888);
                        break;
                    case "4":
                        selected_data = selected_data.slice(3888, 5184);
                        selected_data_0 = selected_data_0.slice(3888, 5184);
                        break;
                    case "5":
                        selected_data = selected_data.slice(5184, 6480);
                        selected_data_0 = selected_data_0.slice(5184, 6480);
                        break;
                    case "6":
                        selected_data = selected_data.slice(6480, 7776);
                        selected_data_0 = selected_data_0.slice(6480, 7776);
                        break;
                    case "7":
                        selected_data = selected_data.slice(7776, 9072);
                        selected_data_0 = selected_data_0.slice(7776, 9072);
                        break;
                    case "8":
                        selected_data = selected_data.slice(9072, 10368);
                        selected_data_0 = selected_data_0.slice(9072, 10368);
                        break;
                    case "9":
                        selected_data = selected_data.slice(10368, 11664);
                        selected_data_0 = selected_data_0.slice(10368, 11664);
                        break;
                    case "10":
                        selected_data = selected_data.slice(11664, 12960);
                        selected_data_0 = selected_data_0.slice(11664, 12960);
                        break;
                    case "11":
                        selected_data = selected_data.slice(12960, 14256);
                        selected_data_0 = selected_data_0.slice(12960, 14256);
                        break;
                    case "12":
                        selected_data = selected_data.slice(14256, 15552);
                        selected_data_0 = selected_data_0.slice(14256, 15552);
                        break;
                    case "13":
                        selected_data = selected_data.slice(15552, 16848);
                        selected_data_0 = selected_data_0.slice(15552, 16848);
                        break;
                    case "14":
                        selected_data = selected_data.slice(16848, 18144);
                        selected_data_0 = selected_data_0.slice(16848, 18144);
                        break;
                    case "15":
                        selected_data = selected_data.slice(18144, 19440);
                        selected_data_0 = selected_data_0.slice(18144, 19440);
                        break;
                        break;
                    case "16":
                        selected_data = selected_data.slice(19440, 20736);
                        selected_data_0 = selected_data_0.slice(19440, 20736);
                        break;

                }
            }
            var array_of_arrays_for_d3 = new Array();
            var array_for_d3 = null;
            var array_of_arrays_for_d3_anomaly_data = new Array();
            var array_for_d3_anomaly_data = null;

            var anomaly_data = selected_data.map(function (item, index) {
                // In this case item correspond to currentValue of array a,
                // using index to get value from array b
                return item - selected_data_0[index];
            });
            var data_min, data_max;
            var anomaly_data_min = Math.min.apply(Math, _toConsumableArray(anomaly_data));
            var anomaly_data_max = Math.max.apply(Math, _toConsumableArray(anomaly_data));
            if (selectParam === 'salinity') {
                data_min = 32;
                data_max = 38;
            } else {
                data_min = Math.min.apply(Math, _toConsumableArray(selected_data));
                data_max = Math.max.apply(Math, _toConsumableArray(selected_data));
            }

            if (selectParam === 'salinity') {
                this.global_data_min = 32;
                this.global_data_max = 38;
            } else {
                this.global_data_min = Math.min(data_min, this.global_data_min);
                this.global_data_max = Math.max(data_max, this.global_data_max);
            }
            this.global_data_anomaly_min = Math.min(anomaly_data_min, this.global_data_anomaly_min);
            this.global_data_anomaly_max = Math.max(anomaly_data_max, this.global_data_anomaly_max);
            ////TODO the same for anomaly DA
            for (var j = 0; j < selected_data.length; j = j + this.gridSize) {
                var start = j;
                array_for_d3 = selected_data.slice(start, start + this.gridSize);
                array_for_d3_anomaly_data = anomaly_data.slice(start, start + this.gridSize);

                array_of_arrays_for_d3.push(array_for_d3);
                array_of_arrays_for_d3_anomaly_data.push(array_for_d3_anomaly_data);
            }

            if (['air_temp', 'humidity', 'pptn', 'evap', 'temp', 'salinity', 'density', 'bathymetry', 'sic_temp', 'sic_cover', 'sic_height'].indexOf(selectParam) > -1) {

                var arrayForHighCharts = this.turnMatrixToHighChartFriendlyArray(array_of_arrays_for_d3);
                var arrayForHighCharts_anomaly_data = this.turnMatrixToHighChartFriendlyArray(array_of_arrays_for_d3_anomaly_data);
            } else {
                var convertedMatrix = this.turnMatrix90DegreesAntiClockwise(array_of_arrays_for_d3);
                var arrayForHighCharts = this.turnMatrixToHighChartFriendlyArray(convertedMatrix);
                var convertedMatrix_anomaly_data = this.turnMatrix90DegreesAntiClockwise(array_of_arrays_for_d3_anomaly_data);
                var arrayForHighCharts_anomaly_data = this.turnMatrixToHighChartFriendlyArray(convertedMatrix_anomaly_data);
            }

            var annual_linegraphDiv = document.getElementById(container);
            var chart = Highcharts.charts[annual_linegraphDiv.getAttribute('data-highcharts-chart')];

            console.log("Mask color" + heatmap_options_for_selected_param['mask_color']);
            chart.update({
                title: {
                    text: 'Instantaneous time slices ' + months_of_the_year[month - 1] + ' ' + year_being_plotted_string
                },
                subtitle: {
                    text: this.decode_heatmap_params[selectParam] + '  ' + heatmap_options_for_selected_param['units']
                },

                colorAxis: {
                    stops: [[0, heatmap_options_for_selected_param['bottom_color']], [0.5, heatmap_options_for_selected_param['middle_color']], [1, heatmap_options_for_selected_param['top_color']]],
                    labels: {
                        format: "{value}"
                    },
                    min: this.global_data_min,
                    max: this.global_data_max
                },
                series: [{
                    data: arrayForHighCharts

                }]
            });
            var anomaly_graph_Div = document.getElementById('plot_container_anomaly_data');
            var anomaly_chart = Highcharts.charts[anomaly_graph_Div.getAttribute('data-highcharts-chart')];

            var zero_stop = Math.abs(this.global_data_anomaly_min / (this.global_data_anomaly_max - this.global_data_anomaly_min));

            anomaly_chart.update({
                title: {
                    text: 'Anomaly data ' + months_of_the_year[month - 1] + ' ' + year_being_plotted_string
                },
                subtitle: {
                    text: this.decode_heatmap_params[selectParam] + '  ' + heatmap_options_for_selected_param['units']
                },
                colorAxis: {
                    stops: [[0, 'rgba(0,0,255,0.5)'], [zero_stop, 'rgba(255,255,255,0.5)'], [1, 'rgba(255,0,0,0.5)']],
                    labels: {
                        format: "{value}"
                    },
                    min: this.global_data_anomaly_min,
                    max: this.global_data_anomaly_max
                },
                series: [{
                    data: arrayForHighCharts_anomaly_data
                }]

            });
        }
    }, {
        key: 'drawGraph',
        value: function drawGraph(container, selectParam, netcdf_data, heatmap_options_for_selected_param) {
            var minColor;
            var maxColor;
            console.log(selectParam);
            var selected_data = netcdf_data[selectParam];

            if (['temp', 'salinity', 'density', 'bathymetry'].indexOf(selectParam) > -1) {
                var ocean_depth_layer = document.getElementById("layerSlider").value;
                selected_data = selected_data.slice(0, 1296);
            }
            var array_of_arrays_for_d3 = new Array();
            var array_for_d3 = null;
            var partOfTheTitle = ''; //selectParam;

            console.log("Array length : " + selected_data.length);
            var data_min = Math.min.apply(Math, _toConsumableArray(selected_data));
            var data_max = Math.max.apply(Math, _toConsumableArray(selected_data));
            this.global_data_min = Math.min(data_min, this.global_data_min);
            this.global_data_max = Math.max(data_max, this.global_data_max);
            this.global_data_anomaly_min = data_min;
            this.global_data_anomaly_max = data_max;
            if (container == 'plot_container') {
                partOfTheTitle = ' Instantaneous time slices';
            } else {
                partOfTheTitle = 'Anomaly data';
            }

            for (var j = 0; j < selected_data.length; j = j + this.gridSize) {
                var start = j;
                array_for_d3 = selected_data.slice(start, start + this.gridSize);

                array_of_arrays_for_d3.push(array_for_d3);
            }

            if (['air_temp', 'humidity', 'pptn', 'evap', 'temp', 'salinity', 'density', 'bathymetry', 'sic_temp', 'sic_cover', 'sic_height'].indexOf(selectParam) > -1) {

                var arrayForHighCharts = this.turnMatrixToHighChartFriendlyArray(array_of_arrays_for_d3);
            } else {
                var convertedMatrix = this.turnMatrix90DegreesAntiClockwise(array_of_arrays_for_d3);
                var arrayForHighCharts = this.turnMatrixToHighChartFriendlyArray(convertedMatrix);
            }

            if (container == 'plot_container') {
                minColor = heatmap_options_for_selected_param['bottom_color']; //'rgba(255,0,0,0.7)';
                maxColor = heatmap_options_for_selected_param['top_color']; //'rgba(0,255,0,0.7)';
            } else {
                minColor = this.anomalyMinColor;
                maxColor = this.anomalyMaxColor;
            }

            Highcharts.chart(container, {

                chart: {
                    type: 'heatmap',
                    plotBackgroundImage: 'assets/images/s397_block04_genie_olay.png',
                    height: 50 / 50 * 100 + '%'
                },

                title: {
                    style: {
                        fontWeight: 'normal',
                        fontSize: 16
                    },
                    text: partOfTheTitle,
                    align: 'low'
                },
                subtitle: {
                    align: 'right',
                    floating: true,
                    fontSize: 26,
                    style: { "color": "#666666", "fontWeight": "bold" },
                    useHTML: true,
                    text: this.decode_heatmap_params[selectParam] + '  ' + heatmap_options_for_selected_param['units'],
                    verticalAlign: 'bottom',
                    y: -45
                },
                colorAxis: {
                    maxColor: minColor,
                    minColor: maxColor,
                    labels: {
                        format: "{value}"
                    },
                    min: data_min,
                    max: data_max
                },
                tooltip: {
                    formatter: function formatter() {
                        return '<b>' + this.point.x + '</b> sold <br><b>' + this.point.value + '</b> items on <br><b>' + this.point.y + '</b>';
                    }
                },

                plotOptions: {
                    heatmap: {
                        nullColor: heatmap_options_for_selected_param['mask_color'],
                        borderBottomColor: 'rgba(255,0,0,0.0)'
                    }
                },

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 450
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            }
                        }
                    }]
                },
                series: [{
                    data: arrayForHighCharts,
                    backgroundImage: 'assets/images/s397_block04_genie_olay.png',
                    fillOpacity: 0.1,
                    borderWidth: 0
                }]
            });
        }
    }, {
        key: 'turnMatrixToHighChartFriendlyArray',
        value: function turnMatrixToHighChartFriendlyArray(convertedMatrix) {
            var requiredArray = [];
            for (var i = 0; i < convertedMatrix.length; i++) {
                var rowElement = convertedMatrix[i];
                for (var j = 0; j < rowElement.length; j++) {
                    if (rowElement[j] == 0) {
                        var newArray = [j, i, null];
                        // var newArray = [j,i,rowElement[j]];
                    }
                    /* else if (rowElement[j]==-99999){
                         var newArray = [j,i,null];
                     }*/
                    else {
                            var newArray = [j, i, rowElement[j]];
                        }

                    requiredArray.push(newArray);
                }
            }
            return requiredArray;
        }
    }, {
        key: 'turnMatrix90DegreesAntiClockwise',
        value: function turnMatrix90DegreesAntiClockwise(originalMatrixArray) {

            var newMatrixArray = new Array();

            var _loop = function _loop(i) {
                var newRaw = new Array();
                originalMatrixArray.forEach(function (element) {
                    newRaw.push(element[i]);
                });
                newMatrixArray.push(newRaw);
            };

            for (var i = 0; i < this.gridSize; i++) {
                _loop(i);
            }
            return newMatrixArray;
        }
    }, {
        key: 'drawMonthlyLinechart',
        value: function drawMonthlyLinechart(title, abriviation) {

            Highcharts.chart('lineseries_div', {

                chart: {
                    title: title + 'Monthly series'
                },

                series: [{
                    name: abriviation,
                    data: []
                }],

                xAxis: {
                    title: {
                        useHTML: true
                    },
                    allowDecimals: false,
                    minTickInterval: 1,
                    labels: {
                        step: 2,
                        formatter: function formatter() {
                            var years = this.value / 12;
                            return years.toFixed(2) + 'years';
                        }
                    }

                },

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 450
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            }
                        }
                    }]
                },

                title: {
                    style: {
                        fontWeight: 'normal',
                        fontSize: 16
                    },
                    text: "Monthly time series"
                }
            });
        }
    }, {
        key: 'drawAnnualLinechart',
        value: function drawAnnualLinechart(title, abriviation) {

            Highcharts.chart('annual_lineseries_div', {

                chart: {
                    title: title + 'Annual time series'
                },
                xAxis: {
                    min: 1,
                    title: {
                        useHTML: true
                    }
                },
                series: [{
                    name: abriviation,
                    data: []
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 450
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            }
                        }
                    }]
                },

                title: {
                    style: {
                        fontWeight: 'normal',
                        fontSize: 16
                    },
                    text: "Annual time series"
                }

            });
        }
    }, {
        key: 'createValuesTables',
        value: function createValuesTables() {
            this.dom.seasonal_values_table_1.id = 'printable_values_table_1';
            this.dom.seasonal_values_table_2.id = 'printable_values_table_2';
            this.dom.annual_averages_table.id = 'annual_averages_table';
            this.dom.forth_row.appendChild(this.dom.annual_averages_table);
            this.dom.fifth_row.appendChild(this.dom.seasonal_values_table_1);
            this.dom.sixth_row.appendChild(this.dom.seasonal_values_table_2);
            var tr_printable_1 = this.dom.seasonal_values_table_1.insertRow(-1);
            var tr_printable_2 = this.dom.seasonal_values_table_2.insertRow(-1);
            var tr_annual = this.dom.annual_averages_table.insertRow(-1);

            tr_annual.style.height = '92px';
            var th_year = document.createElement("th"); // TABLE HEADER.
            th_year.innerHTML = 'Year';
            var th_year_printable_1 = document.createElement("th"); // TABLE HEADER.
            th_year_printable_1.innerHTML = 'Year';
            var th_year_printable_2 = document.createElement("th"); // TABLE HEADER.
            th_year_printable_2.innerHTML = 'Year';
            tr_printable_1.appendChild(th_year_printable_1);
            tr_printable_2.appendChild(th_year_printable_2);
            tr_annual.appendChild(th_year);

            for (var x in this.decode_average_titles) {
                var th = document.createElement("th"); // TABLE HEADER.
                th.innerHTML = this.decode_average_titles[x] + "BLA";
                tr_annual.appendChild(th);
            }

            var i = 0;
            for (var x in this.decode_average_titles) {
                if (i < 10) {
                    var th = document.createElement("th"); // TABLE HEADER.
                    th.innerHTML = this.decode_average_titles[x];
                    tr_printable_1.appendChild(th);
                    i++;
                } else {
                    var th = document.createElement("th"); // TABLE HEADER.
                    th.innerHTML = this.decode_average_titles[x];
                    tr_printable_2.appendChild(th);
                    i++;
                }
            }
        }
    }]);

    return genie_frontend;
}();
